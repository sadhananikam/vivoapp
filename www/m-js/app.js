Date.prototype.AddDays = function (noOfDays) {
    this.setTime(this.getTime() + (noOfDays * (1000 * 60 * 60 * 24)));
    return this;
}
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};  

// Creating modal dialog's DOM
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m" style="width: 300px;">' +
        '<div class="modal-content"style="background-color: rgba(255, 0, 0, 0);box-shadow: none;border: none;">' +

        '<div class="modal-body text-center">' +
        '<svg width="60px" height="60px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ring"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><circle cx="50" cy="50" r="47.5" stroke-dasharray="193.99334635916975 104.45795573186061" stroke="#e62739  " fill="none" stroke-width="5"><animateTransform attributeName="transform" type="rotate" values="0 50 50;180 50 50;360 50 50;" keyTimes="0;0.5;1" dur="1s" repeatCount="indefinite" begin="0s"></animateTransform></circle></svg>' +
        '</div>' +
        '</div></div></div>');

    return {
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Please wait';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null 
            }, options);

            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }

            $dialog.modal();
        },
        hide: function () {
            $dialog.modal('hide');
        }
    };

})(jQuery);

//app starts here
var app = angular.module('vivoMobileApp', ['ui.router', 'ngSanitize', 'ui.select', 'ngAnimate', 'toaster', 'ui-rangeSlider', 'googleplus', 'satellizer', 'tagged.directives.infiniteScroll', 'angularFileUpload']).constant('API_URL','https://www.vivocarat.com/api/v1/');

app.config(['$stateProvider', '$urlRouterProvider', 'GooglePlusProvider', '$authProvider', function ($stateProvider, $urlRouterProvider, GooglePlusProvider, $authProvider) {
    $authProvider.facebook({
        clientId: '235303226837165',
        responseType: 'token'
    });

    $authProvider.google({
        clientId: '491733683292-ic3mvgnhbq4ek4k5gcmgfvhnout2ejvb.apps.googleusercontent.com'
    });

    $authProvider.github({
        clientId: 'GitHub Client ID'
    });

    $authProvider.linkedin({
        clientId: 'LinkedIn Client ID'
    });

    $authProvider.instagram({
        clientId: 'Instagram Client ID'
    });

    $authProvider.yahoo({
        clientId: 'Yahoo Client ID / Consumer Key'
    });

    $authProvider.live({
        clientId: 'Microsoft Client ID'
    });

    $authProvider.twitch({
        clientId: 'Twitch Client ID'
    });

    $authProvider.bitbucket({
        clientId: 'Bitbucket Client ID'
    });

    // No additional setup required for Twitter

    $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        clientId: 'Foursquare Client ID',
        redirectUri: window.location.origin,
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
    });


    // Facebook
    $authProvider.facebook({
        name: 'facebook',
        url: '/api/v1/oauth_login',
        authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
        redirectUri: window.location.origin,
        requiredUrlParams: ['display', 'scope'],
        scope: ['email'],
        scopeDelimiter: ',',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 580,
            height: 400
        }
    });

    // Google
    $authProvider.google({
        url: '/api/v1/oauth_login_google',
        authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        optionalUrlParams: ['display'],
        scope: ['profile', 'email'],
        scopePrefix: 'openid',
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 452,
            height: 633
        }
    });

    // GitHub
    $authProvider.github({
        url: '/auth/github',
        authorizationEndpoint: 'https://github.com/login/oauth/authorize',
        redirectUri: window.location.origin,
        optionalUrlParams: ['scope'],
        scope: ['user:email'],
        scopeDelimiter: ' ',
        type: '2.0',
        popupOptions: {
            width: 1020,
            height: 618
        }
    });

    // Instagram
    $authProvider.instagram({
        name: 'instagram',
        url: '/auth/instagram',
        authorizationEndpoint: 'https://api.instagram.com/oauth/authorize',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        scope: ['basic'],
        scopeDelimiter: '+',
        type: '2.0'
    });

    // LinkedIn
    $authProvider.linkedin({
        url: '/auth/linkedin',
        authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
        redirectUri: window.location.origin,
        requiredUrlParams: ['state'],
        scope: ['r_emailaddress'],
        scopeDelimiter: ' ',
        state: 'STATE',
        type: '2.0',
        popupOptions: {
            width: 527,
            height: 582
        }
    });

    // Twitter
    $authProvider.twitter({
        url: '/auth/twitter',
        authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
        redirectUri: window.location.origin,
        type: '1.0',
        popupOptions: {
            width: 495,
            height: 645
        }
    });

    // Twitch
    $authProvider.twitch({
        url: '/auth/twitch',
        authorizationEndpoint: 'https://api.twitch.tv/kraken/oauth2/authorize',
        redirectUri: window.location.origin,
        requiredUrlParams: ['scope'],
        scope: ['user_read'],
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 500,
            height: 560
        }
    });

    // Windows Live
    $authProvider.live({
        url: '/auth/live',
        authorizationEndpoint: 'https://login.live.com/oauth20_authorize.srf',
        redirectUri: window.location.origin,
        requiredUrlParams: ['display', 'scope'],
        scope: ['wl.emails'],
        scopeDelimiter: ' ',
        display: 'popup',
        type: '2.0',
        popupOptions: {
            width: 500,
            height: 560
        }
    });

    // Yahoo
    $authProvider.yahoo({
        url: '/auth/yahoo',
        authorizationEndpoint: 'https://api.login.yahoo.com/oauth2/request_auth',
        redirectUri: window.location.origin,
        scope: [],
        scopeDelimiter: ',',
        type: '2.0',
        popupOptions: {
            width: 559,
            height: 519
        }
    });

    // Bitbucket
    $authProvider.bitbucket({
        url: '/auth/bitbucket',
        authorizationEndpoint: 'https://bitbucket.org/site/oauth2/authorize',
        redirectUri: window.location.origin + '/',
        optionalUrlParams: ['scope'],
        scope: ['email'],
        scopeDelimiter: ' ',
        type: '2.0',
        popupOptions: {
            width: 1020,
            height: 618
        }
    });

    // Generic OAuth 2.0
    $authProvider.oauth2({
        name: null,
        url: null,
        clientId: null,
        redirectUri: null,
        authorizationEndpoint: null,
        defaultUrlParams: ['response_type', 'client_id', 'redirect_uri'],
        requiredUrlParams: null,
        optionalUrlParams: null,
        scope: null,
        scopePrefix: null,
        scopeDelimiter: null,
        state: null,
        type: null,
        popupOptions: null,
        responseType: 'code',
        responseParams: {
            code: 'code',
            clientId: 'clientId',
            redirectUri: 'redirectUri'
        }
    });

    // Generic OAuth 1.0
    $authProvider.oauth1({
        name: null,
        url: null,
        authorizationEndpoint: null,
        redirectUri: null,
        type: null,
        popupOptions: null
    });
    $urlRouterProvider.otherwise("/i/home");
    $stateProvider
        .state('login/:from', {
            url: '/login/:from',
            templateUrl: 'm-partials/login.html',
            controller: 'loginCtrl'
        })

    .state(':store', {
            url: '/:store',
            templateUrl: 'm-partials/store.html',
            controller: 'storeCtrl'
        }).state('/i/lookbook', {
            url: '/i/lookbook',
            templateUrl: 'm-partials/lookbook.html',
            controller: 'lookBookCtrl'
        })
        .state('/i/lookbookArticle', {
            url: '/i/lookbookArticle/:id',
            templateUrl: 'm-partials/lookbookArticle.html',
            controller: 'lookBookArticleCtrl'
        })
        .state('i/logout', {
            url: '/i/logout',
            templateUrl: 'm-partials/login.html',
            controller: 'logoutCtrl'
        })
        .state('resetpassword/:token', {
            url: '/resetpassword/:token',
            templateUrl: 'm-partials/resetpassword.html',
            controller: 'resetPasswordCtrl'
        })
        .state('i/partner', {
            url: '/i/partner',
            templateUrl: 'm-partials/partner.html',
            controller: 'partnerCtrl'
        })
        .state('/', {
            url: '/i/home',
            templateUrl: 'm-partials/home.html',
            controller: 'authCtrl',

        })
        .state('i/home', {
            url: '/i/home',
            templateUrl: 'm-partials/home.html',
            controller: 'authCtrl',

        }).state('i/returns', {
            url: '/i/returns',
            templateUrl: 'm-partials/returns.html',
            controller: 'returnsCtrl',

        }).state('i/faq', {
            url: '/i/faq',
            templateUrl: 'm-partials/faq.html',
            controller: 'faqCtrl',

        }).state('i/faq/:id', {
            url: '/i/faq/:id',
            templateUrl: 'm-partials/faq.html',
            controller: 'faqCtrl',

        })
        .state('c/:type/:subtype', {
            url: '/c/:type/:subtype',
            templateUrl: 'm-partials/list.html',
            controller: 'categoryCtrl'
        }).state('c/:type', {
            url: '/c/:type',
            templateUrl: 'm-partials/plist.html',
            controller: 'pCategoryCtrl'
        })
        .state('c/store/:store/:type', {
            url: '/c/store/:store/:type',
            templateUrl: 'm-partials/splist.html',
            controller: 'storePCategoryCtrl'
        })
        .state('p/:id', {
            url: '/p/:id',
            templateUrl: 'm-partials/product.html',
            controller: 'productCtrl'
        })
        .state('i/about', {
            url: '/i/about',
            templateUrl: 'm-partials/about.html',
            controller: 'aboutCtrl'
        })
        .state('i/jewelleryeducation', {
            url: '/i/jewelleryeducation',
            templateUrl: 'm-partials/jewelleryeducation.html',
            controller: 'jewelleryEducationCtrl'
        })
        .state('i/ringguide', {
            url: '/i/ringguide',
            templateUrl: 'm-partials/ringguide.html',
            controller: 'ringGuideCtrl'
        })
        .state('i/bangleguide', {
            url: '/i/bangleguide',
            templateUrl: 'm-partials/bangleguide.html',
            controller: 'bangleGuideCtrl'
        })
        .state('i/brands', {
            url: '/i/brands',
            templateUrl: 'm-partials/brands.html',
            controller: 'brandsCtrl'
        })
        .state('i/paymentsuccess/:id', {
            url: '/i/paymentsuccess/:id',
            templateUrl: 'm-partials/paymentsuccess.html',
            controller: 'paymentsuccessCtrl'
        })
        .state('i/jewelleryguide', {
            url: '/i/jewelleryguide',
            templateUrl: 'm-partials/jewelleryguide.html',
            controller: 'jewelleryGuideCtrl'
        })
        .state('i/privacypolicy', {
            url: '/i/privacypolicy',
            templateUrl: 'm-partials/privacypolicy.html',
            controller: 'privacyPolicyCtrl'
        })
        .state('i/careers', {
            url: '/i/careers',
            templateUrl: 'm-partials/careers.html',
            controller: 'careersCtrl'
        })
        .state('i/checkout', {
            url: '/i/checkout',
            templateUrl: 'm-partials/checkout.html',
            controller: 'checkoutCtrl'
        })

    .state('confirmOrder/:orderId', {
            url: '/confirmOrder/:orderId',
            templateUrl: 'm-partials/confirmOrder.html',
            controller: 'confirmOrderCtrl'
        }).state('i/search/:text', {
            url: '/i/search/:text',
            templateUrl: 'm-partials/search.html',
            controller: 'searchCtrl'
        })
        .state('q/:type/:subtype/:q', {
            url: '/q/:type/:subtype/:q',
            templateUrl: 'm-partials/list.html',
            controller: 'queryListCtrl'
        })
        .state('i/wishlist', {
            url: '/i/wishlist',
            templateUrl: 'm-partials/wishlist.html',
            controller: 'wishlistCtrl'
        })
        .state('i/account', {
            url: '/i/account',
            templateUrl: 'm-partials/account.html',
            controller: 'accountCtrl'
        })
        .state('i/editaccount', {
            url: '/i/editaccount',
            templateUrl: 'm-partials/editaccount.html',
            controller: 'editAccountCtrl'
        })
        .state('i/orders', {
            url: '/i/orders',
            templateUrl: 'm-partials/orders.html',
            controller: 'ordersCtrl'
        })
        .state('i/delivery', {
            url: '/i/delivery',
            controller: 'deliveryCtrl',
            templateUrl: 'm-partials/delivery.html'
        }).state('i/terms', {
            url: '/i/terms',
            controller: 'termsCtrl',
            templateUrl: 'm-partials/terms.html'
        }).state('i/contact', {
            url: '/i/contact',
            controller: 'contactCtrl',
            templateUrl: 'm-partials/contact.html'
        })
        .state('i/compare', {
            url: '/i/compare',
            controller: 'compareCtrl',
            templateUrl: 'm-partials/compare.html'
        })
        }]);
app.run(function ($rootScope, $location, Data, $stateParams, $http, $window, $timeout, $timeout, API_URL) {
    $rootScope.testalert = function(){
        alert("test");    
    };
    
    $rootScope.openMenuItem = function (l) {
        alert(l);
    };

    $rootScope.mouseOverCartIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-cart").attr("src", 'images/icons/cart-filled.png');
    }
    $rootScope.mouseOutCartIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-cart").attr("src", 'images/icons/cart-color.png');
    }
    $rootScope.mouseOverShareIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-share").attr("src", 'images/icons/share-filled.png');
    }
    $rootScope.mouseOutShareIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-share").attr("src", 'images/icons/share-color.png');
    }

    $rootScope.mouseOverWishIcon = function (i, ii) {
        $("#" + i + "-" + ii + "-wish").attr("src", 'images/icons/wishlist-filled.png');
    }
    $rootScope.mouseOutWishIcon = function (i, ii) {

        $("#" + i + "-" + ii + "-wish").attr("src", 'images/icons/wishlist.png');
    }
    $rootScope.cartIcon = 'images/mobile/header/icons/cart_bag.png';
    $rootScope.goToLogin = function () {
        $location.path("login/c");
        $('#registerModal').modal('hide');
    }
    $rootScope.couponSignUp = function (coupon) {
        Data.post('couponSignUp', {
            coupon: coupon
        }).then(function (results) {
            Data.toast(results);

        });
    };

    $rootScope.date = new Date();
    $rootScope.isGrid = true;
    $rootScope.setLayout = function (isGrid) {
        $rootScope.isGrid = isGrid;
    }
    $rootScope.addToWatchlist = function (p) {

        if (!$rootScope.authenticated) {

            $('#pleaseLogin').modal('show');
            $timeout(function () {
                $('#pleaseLogin').modal('hide');

            }, 2000);

        } else {

            $http.get('api/v1/addToWatchlist.php?uid=' + $rootScope.uid + '&pid=' + p.id).success(function (data) {
           
                $('#addedToWishList').modal('show');
                $timeout(function () {
                    $('#addedToWishList').modal('hide');

                }, 2000);

            }).error(function (data) {
           
                $('#ErrorplaceOrder').modal('show');
                $timeout(function () {
                    $('#ErrorplaceOrder').modal('hide');

                }, 2000);

            });

        }

    }
    
    $rootScope.addToCart = function (p) {
        if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //                    alert("Please select a braclet size");
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addCartConfirmed(p);
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //                    alert("Please select a ring size");
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addCartConfirmed(p);
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //                alert("Please select a bangle size");
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addCartConfirmed(p);
            }

        } else {

            $rootScope.addCartConfirmed(p);
        }


    }

    $rootScope.addCartConfirmed = function (p) {
        $rootScope.feed = null;
        var isAdded = false;
        var cart = localStorage.getItem('vivo-cart');
        if (cart != null && cart != "") {
            var items = JSON.parse(cart);
            for (var i = 0; i < items.length; i++) {
                if ((items[i].item.VC_SKU == p.VC_SKU) && (items[i].item.VC_SKU_2 == p.VC_SKU_2)) {

                    $("#addToCartAlready").modal('show');

                    $timeout(function () {
                        $("#addToCartAlready").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);

                    isAdded = true;
                }
            }
            if (!isAdded) {
                if(p.quantity !== undefined && p.quantity !== null){
                    items.push({
                        item: p,
                        quantity: p.quantity
                    });
                }else{
                    items.push({
                        item: p,
                        quantity: 1
                    });
                }
                $("#addToCartSuccess").modal('show');

                $timeout(function () {
                    $("#addToCartSuccess").modal('hide');
                    $('.modal-backdrop').remove();
                }, 2000);
            }
            localStorage.setItem('vivo-cart', JSON.stringify(items));
        } else {
            if(p.quantity !== undefined && p.quantity !== null){
                localStorage.setItem('vivo-cart', JSON.stringify([{
                    item: p,
                    quantity: p.quantity
                }]));
            }else{
                localStorage.setItem('vivo-cart', JSON.stringify([{
                    item: p,
                    quantity: 1
                }]));
            }
        }
        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
              
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                    items: JSON.stringify($rootScope.cart),
                    uid: $rootScope.uid
                }  
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }

    $rootScope.subQuantity = function (item, index) {
        if ($rootScope.cart[index].quantity == 1) {
            $rootScope.cart.splice(index, 1);
        } else {
            $rootScope.cart[index].quantity--;
        }
        localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
        
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }
    
    $rootScope.addQuantity = function (item, index) {

        $rootScope.cart[index].quantity++;
        localStorage.setItem('vivo-cart', JSON.stringify($rootScope.cart));
        
        if($rootScope.uid !== undefined && $rootScope.uid !== null){
            $http({
                method: 'POST',
                url : API_URL + 'savetocart',
                data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }
    
    if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
    } else {
        $rootScope.cart = null;
    }
    $rootScope.search = function () {
        $location.path('c/q/' + $rootScope.searchText);
    }
    $rootScope.login = {};
    $rootScope.signup = {};
    
    $rootScope.doLogin = function (customer) {
        waitingDialog.show();
        $http({
            method: 'POST',
            url : API_URL + 'login',
            params : {
                customer: JSON.stringify(customer),
                items: JSON.stringify($rootScope.cart)
            }
        }).then(function successCallback(response){
            waitingDialog.hide();
            var results = response.data;
            $rootScope.response = results.message;
            $('#loggedIn').modal('show');
            $timeout(function () {
                $('#loggedIn').modal('hide');

            }, 2000);
            $rootScope.from = getUrlParameter('from');
            if (results.status == "success") {
                
                if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) 
                        {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;             

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                console.log(p);
                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){     
                                    if ($stateParams.from == 'c') 
                                    {
                                        $location.path('i/checkout');

                                    } else {
                                        $location.path('i/home');
                                    }
                                    $window.location.reload();
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        if ($stateParams.from == 'c') {
                            $location.path('i/checkout');

                        } else {
                            $location.path('i/home');
                        }
                        $window.location.reload();
                    }
            }
        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });
    };
    $rootScope.doOAuthLogin = function (customer) {
        Data.post('oauth_login', {
            customer: customer
        }).then(function (results) {
            alert(results.message);

            if (results.status == "success") {
                if ($stateParams.from == 'c') {
                    $location.path('i/checkout');

                } else {
                    $location.path('i/home');

                }
                location.reload(true);
            }
        });

    };
    $rootScope.signup = {
        email: '',
        password: '',
        name: '',
        phone: '',
        address: ''
    };
    $rootScope.signUp = function (customer) {
        
        waitingDialog.show();
            
        $http({
            method: 'POST',
            url : API_URL + 'signUp',
            params : {
                customer: JSON.stringify(customer),
                items: JSON.stringify($rootScope.cart)
            }
        }).then(function successCallback(response){
            console.log(response.data);
            var results = response.data;
            if (results.status == "error") {
                $rootScope.response = results.message;
                $rootScope.response = results.message;
                $('#loggedIn').modal('show');
                $timeout(function () {
                    $('#loggedIn').modal('hide');

                }, 2000);
            } else {
                if(results.items != null)
                {
                    var pr = JSON.parse(results.items);

                    var k = 1;
                    for (var i = 0; i < pr.length; i++) 
                    {
                        var p_id = pr[i].product_id;
                        var ring_size = pr[i].ring_size;
                        var bangle_size = pr[i].bangle_size;
                        var bracelet_size = pr[i].bracelet_size;
                        var quantity = pr[i].quantity;             console.log(quantity);   

                        $http({
                            method: 'GET',
                            url : API_URL + 'getProductDetail',
                            params : {id:p_id}
                        }).then(function successCallback(response){
                            var p = response.data[0];
                            p.ring_size = pr[k-1].ring_size;
                            p.bangle_size = pr[k-1].bangle_size;
                            p.bracelet_size = pr[k-1].bracelet_size;
                            p.quantity = pr[k-1].quantity;
                            console.log(p);
                            $rootScope.addToCart(p);

                            if(k == pr.length){     
                                location.reload(true);
                            }
                            k = k+1;

                        },function errorCallback(response){
                            console.log(response.data);
                        });

                    }
                }
                else
                {
                    location.reload(true);
                }
            }
            waitingDialog.hide();

        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });
    
    };
    
    $rootScope.couponSignUp = function (coupon) {
        Data.post('couponSignUp', {
            coupon: coupon
        }).then(function (results) {
            alert(results.message);

        });
    };
    $rootScope.logout = function () {
        
        var cart = localStorage.getItem('vivo-cart');
        console.log(cart);
        if(cart != '')
        {
            var items = JSON.parse(cart);
            items=[];
        }
        else{
            var items=[];
        }

        localStorage.setItem('vivo-cart', JSON.stringify(items));

        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        
        Data.get('logout').then(function (results) {
            Data.toast(results);
            location.reload(true);
            
            FB.init({
                appId: '235303226837165',
                //  channelUrl: 'app/channel.html',
                status: true,

                cookie: true,

                xfbml: true
            });

            FB.getLoginStatus(function (response) {
                if (response && response.status === 'connected') {
                    FB.logout(function (response) {
                        location.reload(true);
                    });
                }
            });

        });
    }

    $rootScope
        .$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                $('body').removeClass('no-scroll');
                $timeout(function () {
                    waitingDialog.hide();
                }, 2000);

            });
    $rootScope.$on("$stateChangeStart", function (event, next, current) {
        waitingDialog.show();
        $window.ga('send', 'pageview', {
            page: $location.url()
        });

        $rootScope.authenticated = false;
        Data.get('session').then(function (results) {
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;
                $rootScope.phone = results.phone;
                $rootScope.email = results.email;
                $timeout(function () {
                    if (next.url == "/i/orders") {
                        $rootScope.$broadcast('loadOrderHistoryEvent');

                    }
                    if (next.url == "/i/account" || next.url == "/i/editaccount") {
                        $rootScope.$broadcast('loadAccountDetailsEvent');

                    }
                    if (next.url == "/i/wishlist") {
                        $rootScope.$broadcast('loadWatchlistEvent');
                    }
                    if (next.url == "/confirmOrder/:orderId") {
                        $rootScope.$broadcast('loadConfirmOrderEvent');
                    }
                    if (next.url == "/address/:orderId") {
                        $rootScope.$broadcast('loadAddressEvent');
                    }
                }, 1500);

                var nextUrl = next.url;
                if (nextUrl == '/signup' || nextUrl == '/login/:from') {
                    $location.path("i/home");
                }
            } else {
                $rootScope.$broadcast('loadHomeEvent');
                var nextUrl = next.url;
                if (nextUrl == '/i/wishlist' || nextUrl == '/i/account' || nextUrl == '/i/orders') {
                    $location.path("login/0");
                }
            }
        });
    });
});
app.factory('sAuth', function ($q, $rootScope) {
    return {
        logout: function () {

            var _self = this;

            FB.logout(function (response) {

                $rootScope.$apply(function () {

                    $rootScope.user = _self.user = {};
                    $rootScope.logout();

                });

            });

        },
        getUserInfo: function () {

            var _self = this;

            FB.api('/me?fields=first_name,last_name,gender,email', function (res) {

                $rootScope.$apply(function () {
                    $rootScope.doOAuthLogin({
                        name: res.first_name + res.last_name,
                        email: res.email,
                        gender: res.gender
                    });
                });

            });

        },
        watchLoginChange: function () {

            var _self = this;

            FB.Event.subscribe('auth.login', function (res) {

                if (res.status === 'connected') {
                    _self.getUserInfo();

                } else {

                }

            });
        }
    }
});

app.directive('actualImage', ['$timeout', function ($timeout) {
    return {
        link: function ($scope, element, attrs) {
            function waitForImage(url) {
                var tempImg = new Image();
                tempImg.onload = function () {
                    $timeout(function () {
                        element.attr('src', url);
                    });
                }
                tempImg.src = url;
            }

            attrs.$observe('actualImage', function (newValue) {
                if (newValue) {
                    waitForImage(newValue);
                }
            });
        }
    }
}]);

app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function (item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            out = items;
        }

        return out;
    }
});

//Below code is for INR comma system
app.filter('INR', function () {
    return function (input) {
        if (!isNaN(input)) {
            var currencySymbol = '';         
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;

            if (result.length > 1) {
                output += "." + result[1];
            }

            return currencySymbol + output;
        }
    }
});

app.filter('dateToISO', function() {
  return function(input) {
    input = new Date(input).toISOString();
    return input;
 }});

app.controller('authCtrl', function ($scope, $rootScope, $stateParams, $location, $http, Data, $window, API_URL) {
    
    $scope.buyNow = function (s) {
        $rootScope.addToCart(s);
        $location.path('i/checkout');
    }

    $("#scroller").simplyScroll();
    $scope.coupon = {
        email: null,
        password: null
    }

    $scope.goTo = function (path) {
        $('#categoryEarringsModal').modal('hide');
        $('#categoryBraceletsModal').modal('hide');
        $('#categoryPendantsModal').modal('hide');
        $('#categoryRingsModal').modal('hide');
        $('#categoryNecklacesModal').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
        $location.path(path);
    }
    $scope.signUpCoupon = function () {
        if ($scope.coupon.email == null || $scope.coupon.email == "" || $scope.coupon.password == null || $scope.coupon.password == "") {

            $('#verifydetails').modal('show');
            $timeout(function () {
                $('#verifydetails').modal('hide');

            }, 2000);


        } else {

            $rootScope.couponSignUp($scope.coupon);
            if ($stateParams.from == 'c') {
                $location.path('i/checkout');
            }
            $window.location.reload();
        }

    }
    $scope.showFirstModal = function () {
        if (!$rootScope.authenticated) {
            if (!$rootScope.isCouponModalShown) {
                $rootScope.isCouponModalShown = false;
            }
            if (!$rootScope.isCouponModalShown) {
                $('#registerCouponModal').modal('show');

                $rootScope.isCouponModalShown = true;
            }
        }
    }
    
    $rootScope.$on('loadHomeEvent', function () {

    });
    
     $scope.getTitle = function (id) {
        $scope.id = getUrlParameter('id');
         
         $http({
            method: 'GET',
            url : API_URL + 'getLookbookList'
        }).then(function successCallback(response){
            $scope.title = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $http({
        method: 'GET',
        url : API_URL + 'getVivoHeader',
        params : {p:'home',m:1}
    }).then(function successCallback(response){
        $scope.banners = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getfeaturedproducts'
    }).then(function successCallback(response){
        $scope.list = response.data;
        if (response.data == "invalid") {
        
            $('#invalidUrl').modal('show');
            $timeout(function () {
                $('#invalidUrl').modal('hide');

            }, 2000);

        } else {
            var result = response.data;

        }
    },function errorCallback(response){
        console.log(response.data);
    });
    
    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    
    $scope.getTitle();

});

app.controller("loginCtrl", ["$scope", "$http", "$location", "Data", "$rootScope", "$stateParams", "$window", "$timeout", "sAuth", "GooglePlus", "$auth",'API_URL', function ($scope, $http, $location, Data, $rootScope, $stateParams, $window, $timeout, sAuth, GooglePlus, $auth, API_URL) {

    $scope.customer_new = {
        name: null,
        email: null,
        phone: null,
        password: null
    }
    
    $scope.customer = {
        email: null,
        password: null
    }
    
    $scope.loginGP = function () {
        GooglePlus.login().then(function (authResult) {
            GooglePlus.getUser().then(function (user) {
                $rootScope.doLogin(c);
            });
        }, function (err) {
            console.log(err);
        });
    };
    $scope.loginUser = function (c) {
        if ($scope.customer.email == null || $scope.customer.email == "" || $scope.customer.password == null || $scope.customer.password == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } else {        
            $rootScope.doLogin(c);
        }
    }

    $scope.isPasswordError = false;
    $scope.$watch('customer_new.password', function () {
        if ($scope.customer_new.password !== undefined && $scope.customer_new.password !== null && $scope.customer_new.password.length < 8) {
            $scope.message = "Minimum 8 characters";
            $scope.isPasswordError = true;
        } else {
            $scope.isPasswordError = false;
            $scope.message = "";
        }
    }, true);

    $scope.authenticate = function (provider) {
        $auth.authenticate(provider).then(function (data) {
                if (provider == 'facebook') {
                    $http.get('https://graph.facebook.com/v2.5/me?fields=id,email,first_name,last_name,link,name&access_token=' + data.access_token).success(function (data) {
                        waitingDialog.show();
						$http({
							method: 'POST',
							url : API_URL + 'oauth_login',
							params: {
								customer: data,
								items: JSON.stringify($rootScope.cart)
							}
						}).then(function successCallback(response){			            					
							var results = response.data;
							$rootScope.response = results.message;
							$('#loggedIn').modal('show');
							$timeout(function () {
								$('#loggedIn').modal('hide');

							}, 2000);
							$rootScope.from = getUrlParameter('from');
							if (results.status == "success") {
								if(results.items != null)
								{
									var pr = JSON.parse(results.items);

									var k = 1;
									for (var i = 0; i < pr.length; i++) {
										var p_id = pr[i].product_id;
										var ring_size = pr[i].ring_size;
										var bangle_size = pr[i].bangle_size;
										var bracelet_size = pr[i].bracelet_size;
										var quantity = pr[i].quantity;             console.log(quantity);

										$http({
											method: 'GET',
											url : API_URL + 'getProductDetail',
											params : {id:p_id}
										}).then(function successCallback(response){
											
											var p = response.data[0];
											p.ring_size = pr[k-1].ring_size;
											p.bangle_size = pr[k-1].bangle_size;
											p.bracelet_size = pr[k-1].bracelet_size;
											p.quantity = pr[k-1].quantity;
											console.log(p);
											$rootScope.showmodal = false;
											$rootScope.addToCart(p);
											$rootScope.showmodal = true;

											if(k == pr.length){
												$rootScope.quantity = null;
												if ($rootScope.from == 'c') {
													       
													$location.path('i/checkout');

												} else {
													
													$location.path('i/home');

												}
												$window.location.reload();
											}
											k = k+1;

										},function errorCallback(response){
											console.log(response.data);
										});

									}
								}
								else
								{
									 if ($rootScope.from == 'c') {
										$location.path('i/checkout');

									} else {
										$location.path('i/home');

									}
									$window.location.reload();
								}

							}	
							else
							{
								$location.path('i/home');
								location.reload(true);
							}
						},function errorCallback(response){
							console.log(response.data);
							waitingDialog.hide();
						});

                    }).error(function (data) {


                    });
                }

                if (provider == 'google') {
					waitingDialog.show();
					$http({
						method: 'POST',
						url : API_URL + 'oauth_login_googleplus',
						params: {
							customer: data.config.data,
							items: JSON.stringify($rootScope.cart)
						}
					}).then(function successCallback(response){		
						var results = response.data;
						$rootScope.response = results.message;
						$('#loggedIn').modal('show');
						$timeout(function () {
							$('#loggedIn').modal('hide');

						}, 2000);
						$rootScope.from = getUrlParameter('from');
						if (results.status == "success") {
							if(results.items != null)
							{
								var pr = JSON.parse(results.items);

								var k = 1;
								for (var i = 0; i < pr.length; i++) {
									var p_id = pr[i].product_id;
									var ring_size = pr[i].ring_size;
									var bangle_size = pr[i].bangle_size;
									var bracelet_size = pr[i].bracelet_size;
									var quantity = pr[i].quantity;     

									$http({
										method: 'GET',
										url : API_URL + 'getProductDetail',
										params : {id:p_id}
									}).then(function successCallback(response){
										var p = response.data[0];
										p.ring_size = pr[k-1].ring_size;
										p.bangle_size = pr[k-1].bangle_size;
										p.bracelet_size = pr[k-1].bracelet_size;
										p.quantity = pr[k-1].quantity;
										$rootScope.showmodal = false;
										$rootScope.addToCart(p);
										$rootScope.showmodal = true;

										if(k == pr.length){
											$rootScope.quantity = null;
											if ($rootScope.from == 'c') {
												$location.path('i/checkout');

											} else {
												$location.path('i/home');

											}
											$window.location.reload();
										}
										k = k+1;

									},function errorCallback(response){
										console.log(response.data);
									});

								}
							}
							else
							{
								 if ($rootScope.from == 'c') {
									$location.path('i/checkout');

								} else {
									$location.path('i/home');

								}
								$window.location.reload();
							}

						}	
						else
						{
							$location.path('i/home');
							location.reload(true);
						}
						
					},function errorCallback(response){
						console.log(response.data);
						waitingDialog.hide();
					});

                }

            })
            .catch(function (error) {
                if (error.error) {
                    // Popup error - invalid redirect_uri, pressed cancel button, etc.
                    console.log(error.error);
                } else if (error.data) {
                    // HTTP response error from server
                    console.log(error.data.message, error.status);
                } else {
                    console.log(error);
                }
            });
    };

    $scope.signup = function () {
        if ($scope.customer_new.name == null || $scope.customer_new.name == null || $scope.customer_new.phone == null || $scope.customer_new.phone == null || $scope.customer_new.email == null || $scope.customer_new.email == "" || $scope.customer_new.password == null || $scope.customer_new.password == "") {
            $rootScope.response = "Please Enter the Details";
            $('#verifydetails').modal('show');
            $timeout(function () {
                $('#verifydetails').modal('hide');

            }, 2000);


        } else {
            
            $rootScope.signUp($scope.customer_new);
            if ($stateParams.from == 'c') {
                $location.path('i/checkout');
            }
        }

    }
    $scope.forgotPassword = function () {
        
        if ($scope.femail == null || $scope.femail == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            
            var url = API_URL + 'forgotpassword';
            $http({
                method: 'POST',
                url: url, 
                params: {
                        email: $scope.femail   
                    }
            })
            .then(function successCallback(response){ 
                waitingDialog.hide();
                
                if (response.data.trim() == "ND") {
                    
                    $("#pwdreseterror").modal('show');

                    $timeout(function () {
                        $("#pwdreseterror").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);

                } else {
                    
                    $("#pwdreset").modal('show');

                    $timeout(function () {
                        $("#pwdreset").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);

                }            
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
           });
        }
    }

}]);

app.controller("aboutCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("jewelleryEducationCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);

app.controller("compareCtrl", ['$scope','$rootScope','$http','API_URL', function ($scope, $rootScope, $http, API_URL) {
$scope.p = $rootScope.compare;
    if ($rootScope.compare !== undefined && $rootScope.compare !== null && $rootScope.compare.length > 1) {
        $rootScope.compare = null;
    }
    localStorage.removeItem('vivo-compare'); 

}]);

app.controller("ringGuideCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("bangleGuideCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("brandsCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("partnerCtrl", ["$scope", "$http", "API_URL", function ($scope, $http, API_URL) {
    $scope.resetP = function () {
        $scope.p = {
            pname: null,
            pbrand: null,
            pphone: null,
            pemail: null,
            pmessage: null
        }

    }
    $scope.resetP();

    $scope.addPartner = function () {
        
        waitingDialog.show();
        var url = API_URL + 'partner';
        
        console.log($.param($scope.p));

        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.p),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){
            if (response.data.success == false) {
                // if not successful, bind errors to error variables
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                BootstrapDialog.show({
                    type:BootstrapDialog.TYPE_DANGER,
                    title:'Error',
                    message:msg
                });
                
                waitingDialog.hide();
            } 
            else {            
               waitingDialog.hide();
                $("#addpartner").modal('show');

                $timeout(function () {
                    $("#addpartner").modal('hide');
                }, 2000);

                $scope.resetP();
            }
        },function errorCallback(response){
            console.log(response.data);
            BootstrapDialog.show({
                type:BootstrapDialog.TYPE_DANGER,
                title:'Error',
                message:'An error has occured. Please check the log for details'
            });
            waitingDialog.hide();
        });
    }
    }]);
app.controller("paymentsuccessCtrl", ["$scope", "$http", "$stateParams", '$timeout', 'API_URL', function ($scope, $http, $stateParams, $timeout, API_URL) {
    $scope.id = $stateParams.id;

    $scope.getOrderDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderDetails',
            params : {id:$scope.id}
        }).then(function successCallback(response){
            $scope.order = response.data[0];
            $scope.ship = response.data[1];
            $scope.products = JSON.parse(response.data[0].items);
        },function errorCallback(response){
            console.log(response.data);
            $("#facingerror").modal('show');

            $timeout(function () {
                $("#facingerror").modal('hide');
            }, 2000);
        });
    }
    $scope.getOrderDetails();

}]);

app.controller("jewelleryGuideCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("deliveryCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("termsCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("returnsCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("faqCtrl", ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
        var id = $stateParams.id;
        if(id != undefined)
        {
            var str = "#"+id;
            var currentAttrValue = $(str).attr('href');

            close_accordion_section();

            // Add active class to section title
            $(str).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
            $(str).children(".plusminus").text('-');
        }

        function close_accordion_section() {
            $('.accordion .accordion-section-title').removeClass('active');
            $('.accordion .accordion-section-content').slideUp(50).removeClass('open');
            $('.accordion .accordion-section-title').children(".plusminus").text('+');
        }

        $('.accordion-section-title').click(function(e) {
            // Grab current anchor value
            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_accordion_section();
                $(this).children(".plusminus").text('+');
            }else {
                close_accordion_section();

                // Add active class to section title
                $(this).addClass('active');
                // Open up the hidden content panel
                $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
                $(this).children(".plusminus").text('-');
            }

            e.preventDefault();
        });
        
        function close_accordion_ans() {
            $('.accordion .accordion-section-ques').removeClass('active');
            $('.accordion .accordion-section-ans').slideUp(50).removeClass('open');
            $('.accordion .accordion-section-ques').children(".plusminus").text('+');
        }
        
        $('.accordion-section-ques').click(function(e) {
            // Grab current anchor value
            var currentAttrValue = $(this).attr('href');

            if($(e.target).is('.active')) {
                close_accordion_ans();
                $(this).children(".plusminus").text('+');
            }else {
                close_accordion_ans();

                // Add active class to section title
                $(this).addClass('active');
                // Open up the hidden content panel
                $('.accordion ' + currentAttrValue).slideDown(50).addClass('open'); 
                $(this).children(".plusminus").text('-');
            }

            e.preventDefault();
        });
    
        function openmaincat(id)
        {
            var str = "#"+id;
            var currentAttrValue = $(str).attr('href');

            close_accordion_section();

            // Add active class to section title
            $(str).addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open'); 
            $(str).children(".plusminus").text('-');

            setTimeout(function(){
                $("html,body").animate({
                    scrollTop: $(str).position().top-80
                }, 2000);
            }, 500);
        } 

        $('#Lifetime-ques1').click(function(e){
            openmaincat('Lifetime');
        });

}]);	
app.controller("storeCtrl", ["$scope", "$http", "$stateParams",'API_URL', function ($scope, $http, $stateParams,API_URL) {

    $scope.store = $stateParams.store;

    $http({
        method: 'GET',
        url : API_URL + 'getStoreCategories',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.categoriesList = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });
    
    $http({
        method: 'GET',
        url : API_URL + 'getJewellerInformationByName',
        params : {store:$scope.store}
    }).then(function successCallback(response){
        $scope.jeweller = response.data[0];
    },function errorCallback(response){
        console.log(response.data);
    });

    $http({
        method: 'GET',
        url : API_URL + 'getfeaturedproducts'
    }).then(function successCallback(response){
        $scope.list = response.data;
        if (response.data == "invalid") {
            $('#invalidUrl').modal('show');
            $timeout(function () {
                $('#invalidUrl').modal('hide');

            }, 2000);

        } else {
            var result = response.data;

        }
    },function errorCallback(response){
        console.log(response.data);
    });

}]);

app.controller("privacyPolicyCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("careersCtrl", ["$scope", "$http", function ($scope, $http) {
    }]);
app.controller("lookBookCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location", "API_URL", function ($scope, $http, $stateParams, $rootScope, $location, API_URL) {
    $scope.getTitle = function () {
                                
        $http({
            method: 'GET',
            url : API_URL + 'getlookbook'
        }).then(function successCallback(response){
            $scope.title = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });

    }

    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };
    
    $scope.getTitle();

    }]);

app.controller("lookBookArticleCtrl", ["$scope", "$http", "$stateParams", "$rootScope","API_URL", function ($scope, $http, $stateParams, $rootScope, API_URL) {
 $scope.buyNow = function (p) {
  if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                $location.path('i/checkout');
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                $location.path('i/checkout');
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                $location.path('i/checkout');
            }

        } else {
            $rootScope.addToCart(p);
            $location.path('i/checkout');
        }

    }

    $scope.id = $stateParams.id;
    $http({
        method: 'GET',
        url : API_URL + 'getLookbookDetail',
        params : {id:$scope.id}
    }).then(function successCallback(response){        
        $scope.products = response.data;
        $scope.list = response.data[0];
        $scope.getRelatedBlog();
        $scope.getSimilarProducts();
        if (response.data == "invalid") {
            $("#urlinvalid").modal('show');

            $timeout(function () {
                $("#urlinvalid").modal('hide');
            }, 2000);

        } else {
            var result = response.data;
        }
    },function errorCallback(response){
        console.log(response.data);
    });

    $http({
        method: 'GET',
        url : API_URL + 'getLookbookList'
    }).then(function successCallback(response){
        $scope.title = response.data;
    },function errorCallback(response){
        console.log(response.data);
    });

    $scope.getRelatedBlog = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getRelatedBlog',
            params : {
                id1 : $scope.list.look_1,
                id2 : $scope.list.look_2,
                id3 : $scope.list.look_3
            }
        }).then(function successCallback(response){
            $scope.related = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.getSimilarProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarProducts',
            params : {
                    s1 : $scope.list.similar_1,
                    s2 : $scope.list.similar_2,
                    s3 : $scope.list.similar_3,
                    s4 : $scope.list.similar_4
                }
        }).then(function successCallback(response){
            $scope.slist = response.data;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.readMore = false;
    $scope.toggleReadMore = function () {
        if ($scope.readMore) {
            $scope.readMore = false;
        } else {
            $scope.readMore = true;
        }
    }
    
    //to format date in dd mmmm
    $scope.formatDate = function(date){
          var dateOut = new Date(date);
          return dateOut;
    };

}]);

app.controller('resetPasswordCtrl', function ($scope, $rootScope, $controller, $stateParams, $location, $http, Data, $timeout, API_URL) {
    $scope.validToken = true;
    $scope.token = $stateParams.token;

    $http({
        method: 'GET',
        url : API_URL + 'checkUrl',
        params : {t:$scope.token}
    }).then(function successCallback(response){
        if (response.data.trim() == "invalid") {
            $("#urlinvalid").modal('show');

            $timeout(function () {
                $("#urlinvalid").modal('hide');
            }, 2000);
            $location.path("i/home");

        } else {
            var result = response.data.trim().split('-');
            $scope.uid = result[0];
            $scope.name = result[1];
        }

    },function errorCallback(response){
        console.log(response.data);
    });

    $scope.resetPassword = function (pass) {
        waitingDialog.show();
        var url = API_URL + 'resetPassword';
        $http({
            method: 'POST',
            url: url, 
            params:{
                    uid: $scope.uid,
                    password2: $scope.signup.password2
                },
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){
            waitingDialog.hide();
            
            $("#pwdchanged").modal('show');

            $timeout(function () {
                $("#pwdchanged").modal('hide');
                //$('.modal').modal('hide');
                $('.modal-backdrop').remove();
            }, 2000);

            $location.path("login/0");
            
        },function errorCallback(response){
            console.log(response.data);
            waitingDialog.hide();
        });
    }
});
app.controller("accountCtrl", ["$scope", "$http", "$rootScope", "$timeout", 'API_URL', function ($scope, $http, $rootScope, $timeout, API_URL) {
    $scope.getAccountDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getAccountDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.ad = response.data;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
            $("#acfetcherr").modal('show');

            $timeout(function () {
                $("#acfetcherr").modal('hide');
            }, 2000);
        });
    }
    $rootScope.$on('loadAccountDetailsEvent', function () {
        $scope.getAccountDetails();
    });

}]);

app.controller("editAccountCtrl", ["$scope", "$http", "$rootScope", "$timeout", "$location", 'API_URL', function ($scope, $http, $rootScope, $timeout, $location, API_URL) {
    $scope.updateAccountDetails = function () {
        $scope.address = {
            email: $scope.email,
            uid: $rootScope.uid,
            addId: $scope.ad[1].address_id,
            name: $scope.name,
            phone: $scope.phone,
            first_line: $scope.first_line,
            second_line: $scope.second_line,
            landmark: $scope.landmark,
            street: $scope.street,
            district: $scope.district,
            city: $scope.city,
            state: $scope.state,
            pin: $scope.pin,
            country: $scope.country
        }
        
        $http({
            method: 'POST',
            url : API_URL + 'updateAccountDetails',
            params : {address: JSON.stringify($scope.address)}
        }).then(function successCallback(response){
            $location.path("i/account");
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.getAccountDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getAccountDetails',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            $scope.ad = response.data;
            $scope.name = $scope.ad[0].name;
            $scope.dob = $scope.ad[0].dob;
            $scope.email = $scope.ad[0].email;
            $scope.phone = $scope.ad[0].phone;
            $scope.first_line = $scope.ad[0].first_line;
            $scope.city = $scope.ad[0].city;
            $scope.state = $scope.ad[0].state;
            $scope.pin = $scope.ad[0].pin;
            $scope.country = $scope.ad[0].country;
            $scope.isLoaded = true;
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $rootScope.$on('loadAccountDetailsEvent', function () {
        $scope.getAccountDetails();
    });

}]);

app.controller("ordersCtrl", ["$scope", "$http", "$rootScope", "$timeout", "API_URL", function ($scope, $http, $rootScope, $timeout, API_URL) {
    $scope.isLoaded = false;
    
     $scope.buyNow = function (o) {
        var products = o.products;
        var check = false;
        var k = 1;
        for (var i = 0; i < products.length; i++) 
        {
            var id= products[i].item.id;
            console.log(id);
            var p = null;
            
        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:id}
        }).then(function successCallback(response){
            p = response.data[0];
            
            if(p.bracelet_size == '')
            {
                p.bracelet_size = null;
            }
            if(p.ring_size == '')
            {
                p.ring_size = null;
            }
            if(p.bangle_size == '')
            {
                p.bangle_size = null;
            }
            
            if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                if(k == products.length){
                    window.location.href = "p-checkout.html";
                }
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                if(k == products.length){
                    window.location.href = "p-checkout.html";
                }
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {
                check=true;
                $rootScope.addToCart(p);
                if(k == products.length){
                    window.location.href = "p-checkout.html";
                }
            }

        } else {
            check=true;
            $rootScope.addToCart(p);
            if(k == products.length){
                window.location.href = "p-checkout.html";
            }
        }
            k = k+1;
            
        },function errorCallback(response){
            console.log(response.data);
        }); 
        }
    }
    
    $scope.removeProductFromOrder = function (oId, index, pIndex) {
        var products = JSON.parse($scope.orderHistoryList[pIndex].items);
        products.splice(index, 1);
        $scope.order = {
            oid: oId,
            items: products,
        }

       $http({
            method: 'POST',
            url : API_URL + 'removeProductFromOrder',
            params : {order: JSON.stringify($scope.order)}
        }).then(function successCallback(response){
            $("#orderupdate").modal('show');

            $timeout(function () {
                $("#orderupdate").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.returnOrder = function (id) {
        $scope.rid = id;
        $("#returnModal").modal('show');
    }
    
    $scope.returnOrderConfirm = function (id, reason) {
        
        $http({
            method: 'GET',
            url : API_URL + 'returnOrder',
            params : {oid:id,reason:reason}
        }).then(function successCallback(response){
            $("#returnsuccess").modal('show');

            $timeout(function () {
                $("#returnsuccess").modal('hide');
            }, 2000);

            $("#returnModal").modal('hide');
            $scope.getOrderHistory();

        },function errorCallback(response){
            console.log(response.data);
            $("#returnfail").modal('show');

            $timeout(function () {
                $("#returnfail").modal('hide');
            }, 2000);
        });
        
    }
    
    $scope.cancelOrder = function (id) {
        $http({
            method: 'GET',
            url : API_URL + 'cancelOrder',
            params : {oid:id}
        }).then(function successCallback(response){
            $("#ordercancel").modal('show');

            $timeout(function () {
                $("#ordercancel").modal('hide');
            }, 2000);

            $scope.getOrderHistory();
        },function errorCallback(response){
            console.log(response.data);
            $("#orderfetcherr").modal('show');

            $timeout(function () {
                $("#orderfetcherr").modal('hide');
            }, 2000);
        }); 
    }
    $scope.getOrderHistory = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderHistory',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            if (response.data == "") {
                $scope.orderHistoryList = [];
            } else {
                $scope.orderHistoryList = response.data;
                for (var i = 0; i < response.data.length; i++) {
                    $scope.orderHistoryList[i].products = JSON.parse($scope.orderHistoryList[i].items);
                }
            }
            $scope.isLoaded = true;

        },function errorCallback(response){
            console.log(response.data);
            $("#orderfetchhisterr").modal('show');

            $timeout(function () {
                $("#orderfetchhisterr").modal('hide');
            }, 2000);

        });
    }
    $rootScope.$on('loadOrderHistoryEvent', function () {
        $scope.getOrderHistory();
    });
}]);

app.controller("confirmOrderCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location", "$timeout", function ($scope, $http, $stateParams, $rootScope, $location, $timeout) {
    $scope.$watch('cart', function (newValue, oldValue) {
        $scope.total = 0;
        for (var i = 0; i < $rootScope.cart.length; i++) {
            $scope.total = $scope.total + ($rootScope.cart[i].item.price_after_discount * $rootScope.cart[i].quantity);
        }
    }, true);

    $scope.confirmOrder = function () {

    }

    $rootScope.$on('loadConfirmOrderEvent', function () {
        $scope.getOrderDetails();

    });
    $scope.getOrderDetails = function () {
        $http.get('api/v1/getOrderDetails.php?id=' + $stateParams.orderId).success(function (data) {
            $scope.order = data[0];
            $scope.products = JSON.parse(data[0].items);

        }).error(function (data) {
            $('#ErrorplaceOrder').modal('show');
            $timeout(function () {
                $('#ErrorplaceOrder').modal('hide');

            }, 2000);


        });
    }

    $scope.finalConfirmation = function (amt) {
        $http.get('api/v1/confirmOrder.php?id=' + $stateParams.orderId).success(function (data) {

            $rootScope.response = "Your order has been placed successfully";
            $('#placeOrder').modal('show');
            $timeout(function () {
                $('#placeOrder').modal('hide');

            }, 2000);

            localStorage.setItem('vivo-cart', "");
            $rootScope.cart = null;
            $location.path("i/orders");

        }).error(function (data) {

            $('#ErrorplaceOrder').modal('show');
            $timeout(function () {
                $('#ErrorplaceOrder').modal('hide');

            }, 2000);

        });


    }
}]);

app.controller("contactCtrl", ["$scope", "$http", "$timeout", 'API_URL', function ($scope, $http, $timeout,API_URL) {
    $scope.resetP = function () {
        $scope.p = {
            cname: null,
            cbrand: null,
            cphone: null,
            cemail: null,
            cmessage: null
        }

    }
    $scope.resetP();

    $scope.addContact = function () {
    if ($scope.p.cname == null || $scope.p.cname == "" || $scope.p.cphone == null || $scope.p.cphone == "" || $scope.p.cemail == null || $scope.p.cemail == "") {      
            $('#enterValidDetails').modal('show');
            
            $timeout(function () {
                $('#enterValidDetails').modal('hide');

            }, 2000);    
        } else {
            
            waitingDialog.show();
        var url = API_URL + 'contactus';

        $http({
            method: 'POST',
            url: url, 
            data: $.param($scope.p),
            headers: { 'Content-type':'application/x-www-form-urlencoded' }           
        })
        .then(function successCallback(response){
            if (response.data.success == false) {
                var msg = '';
                if(response.data.errors.name)
                {
                    msg = msg + response.data.errors.name + "\n";
                }
                
                if(response.data.errors.email)
                {
                    msg = msg + response.data.errors.email + "\n";
                }
                
                if(response.data.errors.phone)
                {
                    msg = msg + response.data.errors.phone + "\n";
                }
                
              if(response.data.errors.errorMessage)
                {
                    msg = msg + response.data.errors.errorMessage;
                }
                
                BootstrapDialog.show({
                    type:BootstrapDialog.TYPE_DANGER,
                    title:'Error',
                    message:msg
                });
                
                waitingDialog.hide();
            } 
            else {            
               waitingDialog.hide();
                $('#addpartner').modal('show');
                $timeout(function () {
                    $('#addpartner').modal('hide');

                }, 2000);
                $scope.resetP();
            }
        },function errorCallback(response){
            console.log(response.data);
            BootstrapDialog.show({
                type:BootstrapDialog.TYPE_DANGER,
                title:'Error',
                message:'An error has occured. Please check the log for details'
            });
            waitingDialog.hide();
        });
        
        }
    }

}]);

app.controller("wishlistCtrl", ["$scope", "$http", "$rootScope", "timeout", function ($scope, $http, $rootScope, timeout) {
    $scope.removeFromWatchlist = function (p) {

        if (!$rootScope.authenticated) {
            $('#pleaseLogin').modal('show');
            $timeout(function () {
                $('#pleaseLogin').modal('hide');

            }, 2000);
        } else {

            $http.get('api/v1/removeFromWatchlist.php?uid=' + $rootScope.uid + '&pid=' + p).success(function (data) {
                $('#removedFromWishList').modal('show');
                $timeout(function () {
                    $('#removedFromWishList').modal('hide');

                }, 2000);


                $scope.getWatchlist();
            }).error(function (data) {
                $('#ErrorplaceOrder').modal('show');
                $timeout(function () {
                    $('#ErrorplaceOrder').modal('hide');

                }, 2000);

            });

        }

    }
    $scope.isLoaded = false;

    $scope.getWatchlist = function () {
        $http.get('api/v1/getWatchlist.php?uid=' + $rootScope.uid).success(function (data) {
            $scope.watchlist = data;
            $scope.isLoaded = true;
        }).error(function (data) {
            $('#ErrorplaceOrder').modal('show');
            $timeout(function () {
                $('#ErrorplaceOrder').modal('hide');

            }, 2000);
        });
    }

    $rootScope.$on('loadWatchlistEvent', function () {
        $scope.getWatchlist();
    });

}]);

app.controller("checkoutCtrl", ["$scope", "$http", "$rootScope", "$location", "$window", "Data", "$timeout", '$auth', 'API_URL', function ($scope, $http, $rootScope, $location, $window, Data, $timeout, $auth, API_URL) {
    $scope.code = null;
	$scope.getCombo = function (id) {

        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:id}
        }).then(function successCallback(response){
            $scope.combo = response.data[0];
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.stepNo = 1;
    $scope.customer_new = {
        name: null,
        email: null,
        phone: null,
        password: null
    }
    $scope.customer = {
        email: null,
        password: null
    }
    
    $scope.getallcoupons = function(){
            $http({
                method: 'GET',
                url : API_URL + 'getCoupons'
            }).then(function successCallback(response){
                $scope.allcoupons = response.data;
            },function errorCallback(response){
                console.log(response.data);
            });
    }
    $scope.getallcoupons();
    
    $('#modlgn_passwd').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.loginUser($scope.customer);
        }
    });
    
    $scope.authenticate = function (provider) {
        $auth.authenticate(provider).then(function (data) {
        if (provider == 'facebook') {
            $http.get('https://graph.facebook.com/v2.5/me?fields=id,email,first_name,last_name,link,name&access_token=' + data.access_token).success(function (data) 
            {
                waitingDialog.show();
                $http({
                    method: 'POST',
                    url : API_URL + 'oauth_login',
                    params: {
                        customer: data,
                        items: JSON.stringify($rootScope.cart)
                    }
                }).then(function successCallback(response){
                    var results = response.data;
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.name = results.name;
                    $rootScope.email = results.email;
                    
                    if (results.status == "success") {
                        if(results.items != null)
                        {
                            var pr = JSON.parse(results.items);

                            var k = 1;
                            for (var i = 0; i < pr.length; i++) {
                                var p_id = pr[i].product_id;
                                var ring_size = pr[i].ring_size;
                                var bangle_size = pr[i].bangle_size;
                                var bracelet_size = pr[i].bracelet_size;
                                var quantity = pr[i].quantity;             console.log(quantity);

                                $http({
                                    method: 'GET',
                                    url : API_URL + 'getProductDetail',
                                    params : {id:p_id}
                                }).then(function successCallback(response){
                                    var p = response.data[0];
                                    p.ring_size = pr[k-1].ring_size;
                                    p.bangle_size = pr[k-1].bangle_size;
                                    p.bracelet_size = pr[k-1].bracelet_size;
                                    p.quantity = pr[k-1].quantity;
                                    $rootScope.showmodal = false;
                                    $rootScope.addToCart(p);
                                    $rootScope.showmodal = true;

                                    if(k == pr.length){
                                        Data.get('session').then(function (results) {
                                            $scope.placeOrder();
                                        });
                                    }
                                    k = k+1;
                                    waitingDialog.hide();

                                },function errorCallback(response){
                                    console.log(response.data);
                                    waitingDialog.hide();
                                });

                            }
                        }
                        else
                        {
                             Data.get('session').then(function (results) {
                                $scope.placeOrder();
                            });
                            waitingDialog.hide();
                        }

                    }	
                    else
                    {
                        waitingDialog.hide();
                        $rootScope.response = results.message;
                        $('#loggedIn').modal('show');
                        $timeout(function () {
                            $('#loggedIn').modal('hide');

                        }, 2000);
                    }
                },function errorCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                });

            }).error(function (data) {
            });
        }

        if (provider == 'google') {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'oauth_login_googleplus',
                params: {
                    customer: data.config.data,
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                var results = response.data;
                $rootScope.response = results.message;
                $('#loggedIn').modal('show');
                $timeout(function () {
                    $('#loggedIn').modal('hide');

                }, 2000);
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.name = results.name;
                $rootScope.email = results.email;
                if (results.status == "success") {
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);

                        var k = 1;
                        for (var i = 0; i < pr.length; i++) {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;       

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.showmodal = false;
                                $rootScope.addToCart(p);
                                $rootScope.showmodal = true;

                                if(k == pr.length){
                                    Data.get('session').then(function (results) {
                                        $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                                waitingDialog.hide();

                            },function errorCallback(response){
                                console.log(response.data);
                                waitingDialog.hide();
                            });

                        }
                    }
                    else
                    {
                         Data.get('session').then(function (results) {
                            $scope.placeOrder();
                        });
                        waitingDialog.hide();
                    }

                }	
                else
                {
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    waitingDialog.hide();
                }

            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });

        }

    })
    .catch(function (error) {
        if (error.error) {
            // Popup error - invalid redirect_uri, pressed cancel button, etc.
            console.log(error.error);
        } else if (error.data) {
            // HTTP response error from server
            console.log(error.data.message, error.status);
        } else {
            console.log(error);
        }
    });
    };
    
    $scope.loginUser = function (c) {
        if ($scope.customer.email == null || $scope.customer.email == "" || $scope.customer.password == null || $scope.customer.password == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'login',
                params : {
                    customer: JSON.stringify(c),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                waitingDialog.hide();
                var results = response.data;
                if (results.status == "success") {
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.name = results.name;
                    $rootScope.email = results.email;
                    
                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;       

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){
                                    Data.get('session').then(function (results) {
                                    $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        Data.get('session').then(function (results) {
                            $scope.placeOrder();
                        });
                    }
                } else {
					$rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                }
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });
            
        }
    }

    $scope.isPasswordError = false;
    $scope.$watch('customer_new.password', function () {
        if ($scope.customer_new.password !== undefined && $scope.customer_new.password !== null && $scope.customer_new.password.length < 8) {
            $scope.message = "Minimum 8 characters";
            $scope.isPasswordError = true;
        } else {
            $scope.isPasswordError = false;
            $scope.message = "";
        }
    }, true);

    $scope.signup = function () {
        if ($scope.customer_new.name == null || $scope.customer_new.name == null || $scope.customer_new.phone == null || $scope.customer_new.phone == null || $scope.customer_new.email == null || $scope.customer_new.email == "" || $scope.customer_new.password == null || $scope.customer_new.password == "") {
            $("#verify").modal('show');

            $timeout(function () {
                $("#verify").modal('hide');
            }, 2000);

        } else {
            waitingDialog.show();
            $http({
                method: 'POST',
                url : API_URL + 'signUp',
                params : {
                    customer: JSON.stringify($scope.customer_new),
                    items: JSON.stringify($rootScope.cart)
                }
            }).then(function successCallback(response){
                waitingDialog.hide();
                var results = response.data;
                
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                if (results.status == "error") {
                    $rootScope.response = results.message;
                    $('#loggedIn').modal('show');
                    $timeout(function () {
                        $('#loggedIn').modal('hide');

                    }, 2000);
                } else {

                    if(results.items != null)
                    {
                        var pr = JSON.parse(results.items);
                        
                        var k = 1;
                        for (var i = 0; i < pr.length; i++) 
                        {
                            var p_id = pr[i].product_id;
                            var ring_size = pr[i].ring_size;
                            var bangle_size = pr[i].bangle_size;
                            var bracelet_size = pr[i].bracelet_size;
                            var quantity = pr[i].quantity;       

                            $http({
                                method: 'GET',
                                url : API_URL + 'getProductDetail',
                                params : {id:p_id}
                            }).then(function successCallback(response){
                                var p = response.data[0];
                                p.ring_size = pr[k-1].ring_size;
                                p.bangle_size = pr[k-1].bangle_size;
                                p.bracelet_size = pr[k-1].bracelet_size;
                                p.quantity = pr[k-1].quantity;
                                $rootScope.addToCart(p);
                                
                                if(k == pr.length){     
                                    Data.get('session').then(function (results) {
                                        $rootScope.name = results.name;
                                        $rootScope.email = results.email;
                                        $rootScope.phone = results.phone;
                                        $scope.address.name = $rootScope.name;
                                        $scope.address.phone = $rootScope.phone;
                                        $scope.placeOrder();
                                    });
                                }
                                k = k+1;
                            
                            },function errorCallback(response){
                                console.log(response.data);
                            });

                        }
                    }
                    else
                    {
                        Data.get('session').then(function (results) {
                            $rootScope.name = results.name;
                            $rootScope.email = results.email;
                            $rootScope.phone = results.phone;
                            $scope.address.name = $rootScope.name;
                            $scope.address.phone = $rootScope.phone;
                            $scope.placeOrder();
                        });
                    }
                }
                
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });            
        }

    }
    $scope.forgotPassword = function () {
        if ($scope.femail == null || $scope.femail == "") {
            $("#verifydetails").modal('show');

            $timeout(function () {
                $("#verifydetails").modal('hide');
            }, 2000);

        } 
        else 
        {
            waitingDialog.show();
            var url = API_URL + 'forgotpassword';
            $http({
                method: 'POST',
                url: url, 
                params: {
                        email: $scope.femail   
                    }
            })
            .then(function successCallback(response){
                waitingDialog.hide();
                if (response.data.trim() == "ND") {
                    $("#noemail").modal('show');

                    $timeout(function () {
                        $("#noemail").modal('hide');
                    }, 2000);


                } else {
                    $("#pwdreset").modal('show');

                    $timeout(function () {
                        $("#pwdreset").modal('hide');
                    }, 2000);
                }            
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
           });
        }        
    }

    $scope.address = {
        email: $rootScope.email,
        fline: null,
        uid: $rootScope.uid,
        street: null,
        district: null,
        city: null,
        state: null,
        pin: null,
        country: null,
    };
    $scope.ship_address = {
        email: $rootScope.email,
        fline: null,
        uid: $rootScope.uid,
        street: null,
        district: null,
        city: null,
        state: null,
        pin: null,
        country: null,
    };

    $scope.$watch('address.pin', function () {
        if ($scope.address.pin && $scope.address.pin.length > 5) {
			var city = '';
            var state = '';
            var country = '';
            
            waitingDialog.show();
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.address.pin).success(function(response){
                
                if(response.status == 'OK')
                {      if(response.results[0].address_components.length)
                    {
                        var address_components = response.results[0].address_components;

                        $.each(address_components, function(index, component){
                          var types = component.types;
                            
                          $.each(types, function(index, type){
                            if(type == 'locality') {
                                city = component.long_name;                               
                                return true;
                            }
                            if(type == 'administrative_area_level_1') {
                                state = component.long_name;                            
                                return true;
                            }
                            if(type == 'country') {
                                country = component.long_name;
                                return true;
                            }
                          });
                            
                        });
                        
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.address.city = city;
                                $scope.address.state = state;
                                $scope.address.country = country;
                                waitingDialog.hide();
                            });
                        }, 2000);
                    }
                    else
                    {
                        waitingDialog.hide();
                    }
                }
                else
                {
                    $("#WrongPinCode").modal('show');

                    $timeout(function () {
                        $("#WrongPinCode").modal('hide');
                    }, 2000);
                    waitingDialog.hide();
                }
                
            });
			
        }

    }, true);

    $scope.$watch('ship_address.pin', function () {
        if ($scope.ship_address.pin && $scope.ship_address.pin.length > 5) {
			var city = '';
            var state = '';
            var country = '';
            
            waitingDialog.show();
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$scope.ship_address.pin).success(function(response){
                               
                if(response.status == 'OK')
                {      if(response.results[0].address_components.length)
                    {
                        var address_components = response.results[0].address_components;

                        $.each(address_components, function(index, component){
                          var types = component.types;
                          $.each(types, function(index, type){
                            if(type == 'locality') {
                                city = component.long_name;
                            }
                            if(type == 'administrative_area_level_1') {
                                state = component.long_name;
                            }
                            if(type == 'country') {
                                country = component.long_name;
                            }
                          });
                        });
                        
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ship_address.city = city;
                                $scope.ship_address.state = state;
                                $scope.ship_address.country = country;
                                waitingDialog.hide();
                            });
                        }, 2000);
                    }
                    else
                    {
                        waitingDialog.hide();
                    }
                }
                else
                {
                    $("#WrongPinCode").modal('show');

                    $timeout(function () {
                        $("#WrongPinCode").modal('hide');
                    }, 2000);
                    waitingDialog.hide();
                }               
            });
        }

    }, true);
    $scope.isNewAddress = true;
    $scope.$watch('isNewAddress', function (newValue, oldValue) {
        $scope.address.name = $rootScope.name;
        $scope.address.phone = $rootScope.phone;

        if ($scope.isNewAddress) {
            $scope.address = $scope.ship_address;
        } else {
            $scope.address = {
                email: $rootScope.email,
                fline: null,
                uid: $rootScope.uid,
                city: null,
                state: null,
                pin: null,
                country: null,
                name: $rootScope.name,
                phone: $rootScope.phone

            };
        }
    }, true);


    $scope.addressList = [];
    $scope.getAddresses = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getAddress',
            params : {uid:$rootScope.uid}
        }).then(function successCallback(response){
            if (response.data.length > 0) {

                $scope.ship_address = response.data[0];
                $scope.address = $scope.ship_address;
            }
        },function errorCallback(response){
            console.log(response.data);
        });
    }

    $scope.addAddress = function () {
        waitingDialog.show();
        
        if ($scope.isNewAddress && ($scope.ship_address != null && ($scope.ship_address.name != null && $scope.ship_address.name != "" &&
                $scope.ship_address.phone != null && $scope.ship_address.phone != "" &&
                $scope.ship_address.first_line != null && $scope.ship_address.first_line != "" &&
                $scope.ship_address.pin != null && $scope.ship_address.pin != "" &&
                $scope.ship_address.city != null && $scope.ship_address.city != "" &&
                $scope.ship_address.state != null && $scope.ship_address.state != ""))) {
            $scope.address.email = $rootScope.email;
            $scope.ship_address.email = $rootScope.email;
            $scope.address.uid = $rootScope.uid;
            $scope.address.orderId = $scope.orderId;
            
            $http({
                method: 'POST',
                url : API_URL + 'addNewAddress',
                params : {address: JSON.stringify($scope.address),ship_address: JSON.stringify($scope.ship_address)}
            }).then(function successCallback(response){
                waitingDialog.hide();
                
                $scope.getOrderDetails();
                 $("#collapseThree").collapse('hide');
                $("#collapseFour").collapse('show');
                $("html, body").animate({
                    scrollTop: 0
                }, 800);
                
                $scope.stepNo = 4;
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });

        } else if (!$scope.isNewAddress && ($scope.ship_address != null && ($scope.ship_address.name != null && $scope.ship_address.name != "" &&
                $scope.ship_address.phone != null && $scope.ship_address.phone != "" &&
                $scope.ship_address.first_line != null && $scope.ship_address.first_line != "" &&
                $scope.ship_address.pin != null && $scope.ship_address.pin != "" &&
                $scope.ship_address.city != null && $scope.ship_address.city != "" &&
                $scope.ship_address.state != null && $scope.ship_address.state != "")) && ($scope.address != null && ($scope.address.name != null && $scope.address.name != "" &&
                $scope.address.phone != null && $scope.address.phone != "" &&
                $scope.address.first_line != null && $scope.address.first_line != "" &&
                $scope.address.pin != null && $scope.address.pin != "" &&

                $scope.address.city != null && $scope.address.city != "" &&
                $scope.address.state != null && $scope.address.state != ""))) {


            $scope.address.email = $rootScope.email;
            $scope.ship_address.email = $rootScope.email;
            $scope.address.uid = $rootScope.uid;
            $scope.address.orderId = $scope.orderId;

            $http({
                method: 'POST',
                url : API_URL + 'addNewAddress',
                params : {address: JSON.stringify($scope.address),ship_address: JSON.stringify($scope.ship_address)}
            }).then(function successCallback(response){
                waitingDialog.hide();
                
                $scope.getOrderDetails();
                 $("#collapseThree").collapse('hide');
                $("#collapseFour").collapse('show');
                $("html, body").animate({
                    scrollTop: 0
                }, 800);
                
                $scope.stepNo = 4;
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });

        } else {
            waitingDialog.hide();
            $('#fillShipDetail').modal('show');
            $timeout(function () {
                $('#fillShipDetail').modal('hide');

            }, 2000);
        }

    }

    $scope.confirmOrder = function (total) {
        if ($rootScope.authenticated) {
            $scope.ship_address.name = $rootScope.name;
            $scope.ship_address.phone = $rootScope.phone;
            $scope.placeOrder();

        } else {
            $scope.stepNo = 2;
            $("#collapseOne").collapse('hide');
            $("#collapseTwo").collapse('show');
            $("html, body").animate({
                scrollTop: 0
            }, 800);
        }
    }

    $scope.isCouponApplied = false;
    $scope.applyCode = function () {
        waitingDialog.show();
        if (!$scope.isCouponApplied) {
            
            $http({
                method: 'GET',
                url : API_URL + 'getCoupons'
            }).then(function successCallback(response){
                waitingDialog.hide();
                $scope.coupons = response.data;
                angular.forEach($scope.coupons, function (item, index) {
                    if (!$scope.isCouponApplied) {
                        if ($scope.code == item.coupon_code) {

                            if ($scope.total > item.min_amount) {
                                $scope.codeInt = parseInt(item.coupon_value);
                                $scope.total = $scope.total - $scope.codeInt;
                                $scope.isCouponApplied = true;
                                $scope.disp_code = item.coupon_code;

                            } else {

                                $("#VC500").modal('show');

                                $timeout(function () {
                                    $("#VC500").modal('hide');
                                }, 5000);

                            }
                        }
                    }
                })

                if (!$scope.isCouponApplied) {
                	$scope.code = null;
                    $("#WrongPromoCode").modal('show');

                    $timeout(function () {
                        $("#WrongPromoCode").modal('hide');
                    }, 5000);
                }
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
            });

        }
    }
    
    $('.promocode').bind('keypress', function (e) {
        if (e.keyCode == 13) {
            $scope.applyCode();
            $scope.$apply();
        }
    });

    $scope.$watch('total', function () {
        if ($scope.isCouponApplied) {
            $http({
                method: 'GET',
                url : API_URL + 'getCoupons'
            }).then(function successCallback(response){
                $scope.coupons = response.data;
                angular.forEach($scope.coupons, function (item, index) {

                    if ($scope.code == item.coupon_code) {
                        if (!$scope.isCouponApplied) {
                            if ($scope.total < parseInt(item.min_amount)) {
                                $scope.codeInt = parseInt(item.coupon_value);
                                $scope.isCouponApplied = false;
                                $scope.code = null;
                            }
                        }
                    }
                })
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }, true);
    
    $scope.removeCode = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getCoupons'
        }).then(function successCallback(response){
            $scope.coupons = response.data;
            angular.forEach($scope.coupons, function (item, index) {

                if ($scope.code == item.coupon_code) {

                    if ($scope.total > parseInt(item.min_amount)) {
                        $scope.codeInt = parseInt(item.coupon_value);
                        $scope.total = $scope.total + $scope.codeInt;
                        $scope.isCouponApplied = false;
                        $scope.code = null;
                    }
                }
            })
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.isGift = "0";
    $scope.order = {
        promo: null,
        total: null,
        uid: $rootScope.uid,
        items: $rootScope.cart,
        comment: null,
        isGift: $scope.isGift
    }
    $scope.comment = "";
    $scope.placeOrder = function () {
        $scope.order = {
            promo: $scope.code,
            total: $scope.total,
            uid: $rootScope.uid,
            items: $rootScope.cart,
            comment: $scope.comment,
            isGift: $scope.isGift == "1" ? "1" : "0"
        }

        $http({
            method: 'POST',
            url : API_URL + 'addOrder',
            data: {
                order: JSON.stringify($scope.order)
            }
        }).then(function successCallback(response){
            $scope.orderId = response.data[0].id;
            $scope.getAddresses();
            $("#collapseTwo").collapse('hide');
            $("#collapseThree").collapse('show');
            $("html, body").animate({
                scrollTop: 0
            }, 800);
            $scope.stepNo = 3;
        },function errorCallback(response){
            console.log(response.data);
        });
    }

    $scope.removeFromCart = function (index) {
        var cart = localStorage.getItem('vivo-cart');
        var items = JSON.parse(cart);     
        var p = items[index];

        items.splice(index, 1);
        localStorage.setItem('vivo-cart', JSON.stringify(items));

        $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
        
        if($rootScope.uid != null && $rootScope.uid != ''){
            $http({
                method: 'POST',
                url : API_URL + 'removeFromCart',
                params : {
                    uid:$rootScope.uid,
                    item:JSON.stringify(p.item)  
                }
            }).then(function successCallback(response){
            },function errorCallback(response){
                console.log(response.data);
            });
        }

    }
    $scope.$watch('cart', function (newValue, oldValue) {
        $scope.total = 0;
        if($rootScope.cart != null){
            for (var i = 0; i < $rootScope.cart.length; i++) {
                $scope.total = $scope.total + ($rootScope.cart[i].item.price_after_discount * $rootScope.cart[i].quantity);
            }
        }
    }, true);

    $scope.getOrderDetails = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getOrderDetails',
            params : {id:$scope.orderId}
        }).then(function successCallback(response){
            $scope.order = response.data[0];
            $scope.ship = response.data[1];
            $scope.products = JSON.parse(response.data[0].items);
        },function errorCallback(response){
            console.log(response.data);
            $("#orderfetcherr").modal('show');

            $timeout(function () {
                $("#orderfetcherr").modal('hide');
            }, 2000);
        });
    }

    $scope.isCod = "0";

    $scope.finalConfirmation = function (amt,$event) {
        waitingDialog.show();
        if ($scope.isCod == "1") {
            
            $http({
                method: 'GET',
                url : API_URL + 'confirmOrder',
                params : {
                    id:$scope.orderId,
                    isCod:1,
					isGift: $scope.isGift == "1" ? "1" : "0",
                    txid:'NA',
                    email:$rootScope.email,
                    name:$rootScope.name
                }
            }).then(function successCallback(response){
                $("#ordersuccess").modal('show');

                $timeout(function () {
                    $("#ordersuccess").modal('hide');
                    $('.modal-backdrop').remove();
                }, 2000);

                localStorage.setItem('vivo-cart', "");
                $rootScope.cart = null;
                $location.path("i/paymentsuccess/" + $scope.orderId);
				waitingDialog.hide();
                
            },function errorCallback(response){
                console.log(response.data);
                waitingDialog.hide();
                $("#orderfetcherr").modal('show');

                $timeout(function () {
                    $("#orderfetcherr").modal('hide');
                    $('.modal-backdrop').remove();
                }, 2000);
            });
        } else {
            var options = {

                "key": "rzp_live_ZWUbbMT3Ltg2Hs",
                // "key": "rzp_test_c9GtkbIBKHKLGS",
                "amount": (amt * 100).toString(),
                "name": "Vivocarat Retail Pvt Ltd",
                "description": "Online Jewellery Purchase payment",
                "image": "images/checkout/icons/vivo-logo.png",
                "handler": function (response) {
               
                $http({
                    method: 'GET',
                    url : API_URL + 'confirmOrder',
                    params : {
                        id:$scope.orderId,
                        isCod:0,
						isGift: $scope.isGift == "1" ? "1" : "0",
                        txid:response.razorpay_payment_id,
                        email:$rootScope.email,
                        name:$rootScope.name
                    }
                }).then(function successCallback(response){
                    $("#ordersuccess").modal('show');

                    $timeout(function () {
                        $("#ordersuccess").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);

                    localStorage.setItem('vivo-cart', "");
                    $rootScope.cart = null;
                    $location.path("i/paymentsuccess/" + $scope.orderId);
					waitingDialog.hide();
                },function errorCallback(response){
                    console.log(response.data);
                    waitingDialog.hide();
                    $("#orderfetcherr").modal('show');

                    $timeout(function () {
                        $("#orderfetcherr").modal('hide');
                        $('.modal-backdrop').remove();
                    }, 2000);
                });
                
                },
                "prefill": {

                    "email": $rootScope.email != null ? $rootScope.email : "",
                    "contact": $rootScope.phone != null ? $rootScope.phone : ""
                },
                "notes": {
                    "user_id": $rootScope.uid != null ? $rootScope.uid : "Not Logged in"
                },
                "theme": {
                    "color": "#E62739"
                }
            };
            var rzp1 = new Razorpay(options);
            rzp1.open();
            waitingDialog.hide();
            $event.preventDefault();

        }
    }
}]);

app.controller("storePCategoryCtrl", ["$scope", "$rootScope", "$http", "$stateParams", "$compile", 'API_URL', function ($scope, $rootScope, $http, $stateParams, $compile, API_URL) {

    $scope.category = $stateParams.store;
    $scope.store = $stateParams.store;
    $scope.subtype = $stateParams.type;

    $scope.allN = 0;
    $scope.filterN = 0;
    $scope.sortN = 0;
    
    $scope.getStoreProductsCount = function ()
    {
        $http({
            method: 'GET',
            url : API_URL + 'getStoreProductsCount',
            params : {
                    c: $scope.store,
                    type: $scope.subtype
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.allN = Math.floor($scope.count/8) + 1;
            $scope.allarr = Array.apply(null, {length: $scope.allN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.filterStorePriceCount = function ()
    {
        $http({
            method: 'GET',
            url : API_URL + 'filterStorePriceCount',
            params : {
                c : $scope.store,
                isGold : $scope.isGold, 
                isWhiteGold : $scope.isWhiteGold, isPlatinum : $scope.isPlatinum,
                isRoseGold : $scope.isRoseGold,
                isSilver : $scope.isSilver,
                is22 : $scope.is22, 
                is18 : $scope.is18, 
                is14 : $scope.is14, 
                isOther : $scope.isOther, 
                isCasual : $scope.isCasual, isBridal : $scope.isBridal, isFashion : $scope.isFashion, jewellers : JSON.stringify($scope.jewellers),
                type : $scope.subtype,
                s : $scope.demo1.min,
                e : $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.filterN = Math.floor($scope.count/8) + 1;
            $scope.filterarr = Array.apply(null, {length: $scope.filterN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.sortStoreListCount = function(){
        $http({
            method: 'GET',
            url : API_URL + 'sortStoreListCount',
            params: {
                p: $scope.sortType,
                page: $scope.lastpage,
                c: $scope.store,
                isGold: $scope.isGold,
                isWhiteGold: $scope.isWhiteGold,
                isPlatinum: $scope.isPlatinum,
                isRoseGold: $scope.isRoseGold,
                isSilver: $scope.isSilver,
                is22: $scope.is22,
                is18: $scope.is18,
                is14: $scope.is14,
                isOther: $scope.isOther,
                isCasual: $scope.isCasual,
                isBridal: $scope.isBridal,
                isFashion: $scope.isFashion,
                jewellers: JSON.stringify($scope.jewellers),
                type: $scope.subtype,
                s: $scope.demo1.min,
                e: $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.sortN = Math.floor($scope.count/8) + 1;
            $scope.sortarr = Array.apply(null, {length: $scope.sortN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });  
    }    
    $scope.share = function (p) {
        $('.share-icon').socialShare({
            social: 'facebook,google,twitter',
            shareUrl: 'www.vivocarat.com/%23/p/' + p.id
        });
    }
    $scope.demo1 = {
        min: 20,
        max: 100000
    };

    $scope.getAllJewellers = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getAllJewellers'
        }).then(function successCallback(response){
            var result = response.data;
            var jsonObj = [];
            for(var k =0; k<result.length; k++){
                var j_id = result[k].id;
                var j_name = result[k].name;
                var j_dispname = j_name;

                if(j_name === 'Lagu Bandhu')
                {
                    j_dispname = 'Lagu Bandhu Jewellers';
                }
                if(j_name === 'PP-Gold')
                {
                    j_dispname = 'PP Gold';
                }
                if(j_name === 'Arkina-Diamonds')
                {
                    j_dispname = 'Arkina Diamonds';
                }
                if(j_name === 'ZKD-Jewels')
                {
                    j_dispname = 'ZKD Jewels';
                }
                if(j_name === 'Myrah-Silver-Works')
                {
                    j_dispname = 'Myrah Silver Works';
                }
                if(j_name === 'Charu-Jewels')
                {
                    j_dispname = 'Charu Jewels';
                }

                var item = {};
                item ["jid"] = j_id;
                item ["jname"] = j_dispname;
                item ["jvalue"] = j_name;
                jsonObj.push(item);
            }
            $scope.alljewellers = jsonObj;
            $scope.selectOptions = {
                placeholder: "Select Jewellers..",
                dataTextField: "jname",
                dataValueField: "jvalue",
                valuePrimitive: true,
                autoBind: false,
                dataSource: {
                    type: "jsonp",
                    serverFiltering: true,
                    data:jsonObj
                }
            };
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.getAllJewellers();
    $scope.onCompleteTodo = function(j) {
        if(j.jid === true)
        {
            $scope.jewellers.push(j.jvalue);
        }
        else if(j.jid === false)
        {
            var index = $scope.jewellers.indexOf(j.jvalue);
            $scope.jewellers.splice(index, 1);
        }
   };

    $scope.resetFilter = function () {
        $scope.selectedIds = [];
        $scope.isFirst = 1;
        $scope.jewellers = [];
        $scope.isGold = false;
        $scope.isWhiteGold = false;
        $scope.isPlatinum = false;
        $scope.isRoseGold = false;
        $scope.isSilver = false;
        $scope.is22 = false;
        $scope.is18 = false;
        $scope.is14 = false;
        $scope.isOther = false;
        $scope.isCasual = false;
        $scope.isBridal = false;
        $scope.isFashion = false;
        $scope.demo1 = {
            min: 20,
            max: 100000
        };
        angular.forEach($scope.alljewellers, function (j) {

            j.jid = false;
        });
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.getStoreProductsCount();
    }
    $scope.resetFilter();
    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter != null) {
            $rootScope.closeSortSm();
            $rootScope.closeSortXs();
            if ($scope.selectedFilter == "Price H to L") {
                $scope.sortType = "price_after_discount desc";
            } else if ($scope.selectedFilter == "Price L to H") {
                $scope.sortType = "price_after_discount asc";
            } else if ($scope.selectedFilter == "Name") {
                $scope.sortType = "title desc";
            }else if ($scope.selectedFilter == "") {
                $scope.sortType = "id desc";
            }
            $scope.isFirst = 3;
            $scope.lastpage = 0;
            $scope.resultList = [];
            $scope.sortStoreListCount();
        }
    }, true);

    $scope.banImg = $scope.category + "_" + $scope.subtype;
    $scope.list = [];
    $scope.isLoaded = false;

    var el = angular.element(document.querySelector('#loadMore'));
    el.attr('tagged-infinite-scroll', 'loadMore()');
    $compile(el)($scope);
    $scope.lastpage = 0;
    $scope.resultList = [];
    $scope.isFirst = 1;

    $scope.loadMore = function () {

        $scope.fetching = true; 

        if ($scope.isFirst == 1 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.allarr )>-1 && typeof $scope.allarr !== 'undefined' && $scope.allarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.allarr );
                $scope.allarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'getStoreProducts',
                    params: {
                        c: $scope.store,
                        type: $scope.subtype,
                        page: $scope.lastpage
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                        $scope.isLoaded = true;
                    };
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
          
        } else if ($scope.isFirst == 2 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.filterarr )>-1 && typeof $scope.filterarr !== 'undefined' && $scope.filterarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.filterarr );
                $scope.filterarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'filterStorePrice',
                    params: {
                        page: $scope.lastpage,
                        c: $scope.store,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;

                    } else {

                        $scope.disabled = true; 
                    }             
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
                 
        } else if ($scope.isFirst == 3 && $scope.disabled == false) {            
            if($.inArray( $scope.lastpage, $scope.sortarr )>-1 && typeof $scope.sortarr !== 'undefined' && $scope.sortarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.sortarr );
                $scope.sortarr.splice(pos, 1);
            
                $http({
                    method: 'GET',
                    url : API_URL + 'sortStoreList',
                    params: {
                        p: $scope.sortType,
                        page: $scope.lastpage,
                        c: $scope.store,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                    }

                },function errorCallback(response){
                    console.log(response.data);
                });  
            }
            
        }
    };

    $scope.filter = function () {
        $scope.isFirst = 2;
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.isFilter = false;
        $scope.filterStorePriceCount();
    }

    $scope.clearFilter = function () {
        $scope.lastpage = 0;
        $scope.isFilter = false;
        $scope.resetFilter();
    }
}]);

app.controller("pCategoryCtrl", ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
    $scope.demo1 = {
        min: 20,
        max: 1000000
    };

    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter == "Price H to L") {
            $scope.reverse = true;
            $scope.predicate = "price_after_discount desc";
        } else if ($scope.selectedFilter == "Price L to H") {
            $scope.reverse = false;
            $scope.predicate = "price_after_discount asc";
        } else if ($scope.selectedFilter == "Name") {
            $scope.reverse = "title desc";
            $scope.predicate = predicate;
        }
    }, true);
    $scope.category = $stateParams.type;

    $scope.banImg = $scope.category;
    $http.get('api/v1/getParentProducts.php?c=' + $scope.category).success(function (data) {
        $scope.list = data;
        $scope.resultList = [];
        var i, j, chunk = 18;
        for (i = 0, j = $scope.list.length; i < j; i += chunk) {

            $scope.resultList.push($scope.list.slice(i, i + chunk));
        }
        $scope.isLoaded = true;
    }).error(function (data) {
        $('#ErrorplaceOrder').modal('show');
        $timeout(function () {
            $('#ErrorplaceOrder').modal('hide');

        }, 2000);

    });

    $scope.filter = function () {
        $http.get('api/v1/filterParentPrice.php?c=' + $scope.category + '&s=' + $scope.demo1.min + '&e=' + $scope.demo1.max).success(function (data) {
            $scope.list = data;
        }).error(function (data) {
            $('#ErrorplaceOrder').modal('show');
            $timeout(function () {
                $('#ErrorplaceOrder').modal('hide');

            }, 2000);

        });
    }
    }]);

app.controller("queryListCtrl", ["$scope", "$http", "$stateParams",  function ($scope, $http, $stateParams) {
    $scope.q = $stateParams.q;
    $scope.share = function (p) {
        $('.share-icon').socialShare({
            social: 'facebook,google,twitter',
            shareUrl: 'www.vivocarat.com/%23/p/' + p.id
        });
    }
    $scope.demo1 = {
        min: 20,
        max: 100000
    };
    $scope.selectOptions = {
        placeholder: "Select Jewellers..",
        dataTextField: "name",
        dataValueField: "name",
        valuePrimitive: true,
        autoBind: false,
        dataSource: {
            type: "jsonp",
            serverFiltering: true,
            data: [{
                name: "Megha Jewellers"
            }, {
                name: "Mayura Jewellers"
            }, {
                name: "Kundan Jewellers"
            }, {
                name: "Shankaram Jewellers"
            }, {
                name: "Lagu Bandhu"
            }, {
                name: "Mani Jewellers"
            }, {
                name: "Incocu Jewellers"
            }, {
                name: "Regaalia Jewels"
            }]

        }
    };
    $scope.resetFilter = function () {
        $scope.selectedIds = [];
        $scope.jewellers = [];
        $scope.isGold = false;
        $scope.isWhiteGold = false;
        $scope.isPlatinum = false;
        $scope.isSilver = false;
        $scope.is22 = false;
        $scope.is18 = false;
        $scope.is14 = false;
        $scope.isOther = false;
        $scope.isCasual = false;
        $scope.isBridal = false;
        $scope.isFashion = false;
        $scope.demo1 = {
            min: 20,
            max: 100000
        };
    }
    $scope.resetFilter();
    $scope.openFilter = function () {
        $("#filterArea").slideDown("slow", function () {

        });
    }
    $scope.closeFilter = function () {
        $("#filterArea").slideUp("slow", function () {

        });
    }
    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter != null) {
            if ($scope.selectedFilter == "Price H to L") {
                $scope.sortType = "price_after_discount desc";
            } else if ($scope.selectedFilter == "Price L to H") {
                $scope.sortType = "price_after_discount asc";
            } else if ($scope.selectedFilter == "Name") {
                $scope.sortType = "title desc";
            }

            $http.get('api/v1/filterList.php?' + $scope.q).success(function (data) {
                $scope.list = data;

                $scope.resultList = data;

                $scope.isLoaded = true;
            }).error(function (data) {
                $("#jewelerr").modal('show');

                $timeout(function () {
                    $("#orderfetcherr").modal('hide');
                }, 2000);

            });
        }
    }, true);
    $scope.category = $stateParams.type;
    $scope.subtype = $stateParams.subtype;
    $scope.banImg = $scope.category + "_" + $scope.subtype;
    $scope.list = [];
    $scope.isLoaded = false;
    $http.get('api/v1/filterPrice.php?' + $scope.q).success(function (data) {
        $scope.list = data;

        $scope.resultList = data;

        $scope.isLoaded = true;
    }).error(function (data) {
        $("#jewelerr").modal('show');

        $timeout(function () {
            $("#jewelerr").modal('hide');
        }, 2000);

    });

    $scope.filter = function () {
        $http.get('api/v1/filterPrice.php?c=' + $scope.category +
            '&isGold=' + $scope.isGold +
            '&isWhiteGold=' + $scope.isWhiteGold +
            '&isPlatinum=' + $scope.isPlatinum +
            '&isSilver=' + $scope.isSilver +
            '&is22=' + $scope.is22 +
            '&is18=' + $scope.is18 +
            '&is14=' + $scope.is14 +
            '&isOther=' + $scope.isOther +
            '&isCasual=' + $scope.isCasual +
            '&isBridal=' + $scope.isBridal +
            '&isFashion=' + $scope.isFashion +
            '&jewellers=' + JSON.stringify($scope.jewellers) +
            '&type=' + $scope.subtype + '&s=' + $scope.demo1.min + '&e=' + $scope.demo1.max).success(function (data) {
            $scope.list = data;

            $scope.resultList = data;

            $scope.isLoaded = true;
        }).error(function (data) {
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);

        });
    }
    }]);

app.controller("searchCtrl", ["$scope", "$rootScope", "$http", "$stateParams", "$compile", "API_URL", function ($scope , $rootScope, $http, $stateParams, $compile, API_URL) {
    $scope.allN = 0;
    $scope.filterN = 0;
    $scope.sortN = 0;  
    
    $scope.q = $stateParams.text;
    
    $scope.resultfor = $scope.q.replace("+",",");
    
    $scope.getProductsTagCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'getProductsTagCount',
            params : {
                c: $scope.category,
                tag: $scope.q,
                type: $scope.subtype,
                page: $scope.lastpage
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.allN = Math.floor($scope.count/8) + 1;
            $scope.allarr = Array.apply(null, {length: $scope.allN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.filterPriceTagCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'filterPriceTagCount',
            params : {
                    c : $scope.category,
                    isGold : $scope.isGold,
                    isWhiteGold : $scope.isWhiteGold,
                    isPlatinum : $scope.isPlatinum,
                    isRoseGold : $scope.isRoseGold,
                    isSilver : $scope.isSilver,
                    is22 : $scope.is22,
                    is18 : $scope.is18,
                    is14 : $scope.is14,
                    isOther : $scope.isOther,
                    isCasual : $scope.isCasual,
                    isBridal : $scope.isBridal,
                    isFashion : $scope.isFashion,
                    jewellers : JSON.stringify($scope.jewellers),
                    type : $scope.subtype,
                    s : $scope.demo1.min,
                    e : $scope.demo1.max,
                    tag : $scope.q
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.filterN = Math.floor($scope.count/8) + 1;
            $scope.filterarr = Array.apply(null, {length: $scope.filterN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.sortListTagCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'sortListTagCount',
            params: {
                tag : $scope.q,
                p: $scope.sortType,
                page: $scope.lastpage,
                c: $scope.category,
                isGold: $scope.isGold,
                isWhiteGold: $scope.isWhiteGold,
                isPlatinum: $scope.isPlatinum,
                isRoseGold: $scope.isRoseGold,
                isSilver: $scope.isSilver,
                is22: $scope.is22,
                is18: $scope.is18,
                is14: $scope.is14,
                isOther: $scope.isOther,
                isCasual: $scope.isCasual,
                isBridal: $scope.isBridal,
                isFashion: $scope.isFashion,
                jewellers: JSON.stringify($scope.jewellers),
                type: $scope.subtype,
                s: $scope.demo1.min,
                e: $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.sortN = Math.floor($scope.count/8) + 1;
            $scope.sortarr = Array.apply(null, {length: $scope.sortN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.share = function (p) {
        $('.share-icon').socialShare({
            social: 'facebook,google,twitter',
            shareUrl: 'www.vivocarat.com/%23/p/' + p.id
        });
    }
    $scope.demo1 = {
        min: 20,
        max: 100000
    };

    $scope.getAllJewellers = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getAllJewellers'
        }).then(function successCallback(response){
            var result = response.data;
            var jsonObj = [];
            for(var k =0; k<result.length; k++){
                var j_id = result[k].id;
                var j_name = result[k].name;
                var j_dispname = j_name;

                if(j_name === 'Lagu Bandhu')
                {
                    j_dispname = 'Lagu Bandhu Jewellers';
                }
                if(j_name === 'PP-Gold')
                {
                    j_dispname = 'PP Gold';
                }
                if(j_name === 'Arkina-Diamonds')
                {
                    j_dispname = 'Arkina Diamonds';
                }
                if(j_name === 'ZKD-Jewels')
                {
                    j_dispname = 'ZKD Jewels';
                }
                if(j_name === 'Myrah-Silver-Works')
                {
                    j_dispname = 'Myrah Silver Works';
                }
                if(j_name === 'Charu-Jewels')
                {
                    j_dispname = 'Charu Jewels';
                }

                var item = {};
                item ["jid"] = j_id;
                item ["jname"] = j_dispname;
                item ["jvalue"] = j_name;
                jsonObj.push(item);
            }
            $scope.alljewellers = jsonObj;

            $scope.selectOptions = {
                placeholder: "Select Jewellers..",
                dataTextField: "jname",
                dataValueField: "jvalue",
                valuePrimitive: true,
                autoBind: false,
                dataSource: {
                    type: "jsonp",
                    serverFiltering: true,
                    data:jsonObj
                }
            };
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.getAllJewellers();
    $scope.onCompleteTodo = function(j) {

        if(j.jid === true)
        {
            $scope.jewellers.push(j.jvalue);
        }
        else if(j.jid === false)
        {
            var index = $scope.jewellers.indexOf(j.jvalue);
            $scope.jewellers.splice(index, 1);
        }
   };

    $scope.resetFilter = function () {
        $scope.selectedIds = [];
        $scope.isFirst = 1;
        $scope.jewellers = [];
        $scope.isGold = false;
        $scope.isWhiteGold = false;
        $scope.isPlatinum = false;
        $scope.isRoseGold = false;
        $scope.isSilver = false;
        $scope.is22 = false;
        $scope.is18 = false;
        $scope.is14 = false;
        $scope.isOther = false;
        $scope.isCasual = false;
        $scope.isBridal = false;
        $scope.isFashion = false;
        $scope.demo1 = {
            min: 20,
            max: 100000
        };
        angular.forEach($scope.alljewellers, function (j) {

            j.jid = false;
        });
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.getProductsTagCount();
    }
    $scope.resetFilter();
    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter != null) {
            $rootScope.closeSortSm();
            $rootScope.closeSortXs();
            if ($scope.selectedFilter == "Price H to L") {
                $scope.sortType = "price_after_discount desc";
            } else if ($scope.selectedFilter == "Price L to H") {
                $scope.sortType = "price_after_discount asc";
            } else if ($scope.selectedFilter == "Name") {
                $scope.sortType = "title desc";
            }else if ($scope.selectedFilter == "") {
                $scope.sortType = "id desc";
            }
            $scope.isFirst = 3;
            $scope.lastpage = 0;
            $scope.resultList = [];
            $scope.sortListTagCount();
        }
    }, true);
    $scope.category = $stateParams.type;
    $scope.subtype = $stateParams.subtype;
    $scope.banImg = "images/mobile/search/" + $scope.q + ".png";
    $scope.defaultImage = "images/mobile/search/default-search-banner.png";
    $scope.list = [];
    $scope.isLoaded = false;
    
    var el = angular.element(document.querySelector('#loadMore'));
    el.attr('tagged-infinite-scroll', 'loadMore()');
    $compile(el)($scope);
    $scope.lastpage = 0;
    $scope.resultList = [];
    $scope.isFirst = 1;

    $scope.loadMore = function () {

        $scope.fetching = true; 

        if ($scope.isFirst == 1 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.allarr )>-1 && typeof $scope.allarr !== 'undefined' && $scope.allarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.allarr );
                $scope.allarr.splice(pos, 1);
                
               $http({
                    method: 'GET',
                    url : API_URL + 'getProductsTag',
                    params: {
                        c: $scope.category,
                        tag: $scope.q,
                        type: $scope.subtype,
                        page: $scope.lastpage
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                        $scope.isLoaded = true;
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        } else if ($scope.isFirst == 2 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.filterarr )>-1 && typeof $scope.filterarr !== 'undefined' && $scope.filterarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.filterarr );
                $scope.filterarr.splice(pos, 1);
                
                $http({
                    method: 'GET',
                    url : API_URL + 'filterPriceTag',
                    params: {
                        tag: $scope.q,
                        page: $scope.lastpage,
                        c: $scope.category,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        } else if ($scope.isFirst == 3 && $scope.disabled == false) {            
            if($.inArray( $scope.lastpage, $scope.sortarr )>-1 && typeof $scope.sortarr !== 'undefined' && $scope.sortarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.sortarr );
                $scope.sortarr.splice(pos, 1);
                
                $http({
                    method: 'GET',
                    url : API_URL + 'sortListTag',
                    params: {
                        tag : $scope.q,
                        p: $scope.sortType,
                        page: $scope.lastpage,
                        c: $scope.category,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        }

    };

    $scope.filter = function () {
        $scope.isFirst = 2;
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.isFilter = false;
        $scope.filterPriceTagCount();
    }

    $scope.clearFilter = function () {
        $scope.lastpage = 0;
        $scope.isFilter = false;
        $scope.resetFilter();
    }
}]);


app.controller("categoryCtrl", ["$scope", "$rootScope", "$http", "$stateParams", "$compile", "API_URL", function ($scope, $rootScope, $http, $stateParams, $compile, API_URL) {

    $scope.allN = 0;
    $scope.filterN = 0;
    $scope.sortN = 0;  
    
    $scope.category = $stateParams.type;
    $scope.subtype = $stateParams.subtype;
    
    $scope.getProductsCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'getProductsCount',
            params : {
                    c : $scope.category,
                    type : $scope.subtype
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.allN = Math.floor($scope.count/8) + 1;
            $scope.allarr = Array.apply(null, {length: $scope.allN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.filterPriceCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'filterPriceCount',
            params : {
                    c : $scope.category,
                    isGold : $scope.isGold,
                    isWhiteGold : $scope.isWhiteGold,
                    isPlatinum : $scope.isPlatinum,
                    isRoseGold : $scope.isRoseGold,
                    isSilver : $scope.isSilver,
                    is22 : $scope.is22,
                    is18 : $scope.is18,
                    is14 : $scope.is14,
                    isOther : $scope.isOther,
                    isCasual : $scope.isCasual, isBridal : $scope.isBridal, isFashion : $scope.isFashion, jewellers : JSON.stringify($scope.jewellers),
                    type : $scope.subtype,
                    s : $scope.demo1.min,
                    e : $scope.demo1.max
                }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.filterN = Math.floor($scope.count/8) + 1;
            $scope.filterarr = Array.apply(null, {length: $scope.filterN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.sortListCount = function (){
        $http({
            method: 'GET',
            url : API_URL + 'sortListCount',
            params : {
                p : $scope.sortType,
                c : $scope.category,
                isGold : $scope.isGold,
                isWhiteGold : $scope.isWhiteGold,
                isPlatinum : $scope.isPlatinum,
                isRoseGold : $scope.isRoseGold,
                isSilver : $scope.isSilver,
                is22 : $scope.is22,
                is18 : $scope.is18,
                is14 : $scope.is14,
                isOther : $scope.isOther,
                isCasual : $scope.isCasual,
                isBridal : $scope.isBridal,
                isFashion : $scope.isFashion,
                jewellers : JSON.stringify($scope.jewellers),
                type : $scope.subtype,
                s : $scope.demo1.min,
                e : $scope.demo1.max
            }
        }).then(function successCallback(response){
            $scope.count = response.data[0].count;
            $scope.sortN = Math.floor($scope.count/8) + 1;
            $scope.sortarr = Array.apply(null, {length: $scope.sortN}).map(Number.call, Number);
            $scope.disabled = false;
            $scope.loadMore();
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    
    $scope.share = function (p) {
        $('.share-icon').socialShare({
            social: 'facebook,google,twitter',
            shareUrl: 'www.vivocarat.com/%23/p/' + p.id
        });
    }
    $scope.demo1 = {
        min: 20,
        max: 100000
    };

    $scope.getAllJewellers = function(){
        $http({
            method: 'GET',
            url : API_URL + 'getAllJewellers'
        }).then(function successCallback(response){
            var result = response.data;
            var jsonObj = [];
            for(var k =0; k<result.length; k++){
                var j_id = result[k].id;
                var j_name = result[k].name;
                var j_dispname = j_name;

                if(j_name === 'Lagu Bandhu')
                {
                    j_dispname = 'Lagu Bandhu Jewellers';
                }
                if(j_name === 'PP-Gold')
                {
                    j_dispname = 'PP Gold';
                }
                if(j_name === 'Arkina-Diamonds')
                {
                    j_dispname = 'Arkina Diamonds';
                }
                if(j_name === 'ZKD-Jewels')
                {
                    j_dispname = 'ZKD Jewels';
                }
                if(j_name === 'Myrah-Silver-Works')
                {
                    j_dispname = 'Myrah Silver Works';
                }
                if(j_name === 'Charu-Jewels')
                {
                    j_dispname = 'Charu Jewels';
                }

                var item = {};
                item ["jid"] = j_id;
                item ["jname"] = j_dispname;
                item ["jvalue"] = j_name;
                jsonObj.push(item);
            }
            $scope.alljewellers = jsonObj;
            $scope.selectOptions = {
                placeholder: "Select Jewellers..",
                dataTextField: "jname",
                dataValueField: "jvalue",
                valuePrimitive: true,
                autoBind: false,
                dataSource: {
                    type: "jsonp",
                    serverFiltering: true,
                    data:jsonObj
                }
            };
        },function errorCallback(response){
            console.log(response.data);
        });
    }
    $scope.getAllJewellers();
    $scope.onCompleteTodo = function(j) {
        if(j.jid === true)
        {
            $scope.jewellers.push(j.jvalue);
        }
        else if(j.jid === false)
        {
            var index = $scope.jewellers.indexOf(j.jvalue);
            $scope.jewellers.splice(index, 1);
        }
   };

    $scope.resetFilter = function () {
        $scope.selectedIds = [];
        $scope.isFirst = 1;
        $scope.jewellers = [];
        $scope.isGold = false;
        $scope.isWhiteGold = false;
        $scope.isPlatinum = false;
        $scope.isRoseGold = false;
        $scope.isSilver = false;
        $scope.is22 = false;
        $scope.is18 = false;
        $scope.is14 = false;
        $scope.isOther = false;
        $scope.isCasual = false;
        $scope.isBridal = false;
        $scope.isFashion = false;
        $scope.demo1 = {
            min: 20,
            max: 100000
        };
        angular.forEach($scope.alljewellers, function (j) {

            j.jid = false;
        });
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.getProductsCount();
    }
    $scope.resetFilter();
    $scope.openFilter = function () {
        $("#filterArea").slideDown("slow", function () {
        });
    }
    $scope.closeFilter = function () {
        $("#filterArea").slideUp("slow", function () {
        });
    }
    $scope.$watch('selectedFilter', function () {
        if ($scope.selectedFilter != null) {
            $rootScope.closeSortSm();
            $rootScope.closeSortXs();
            if ($scope.selectedFilter == "Price H to L") {
                $scope.sortType = "price_after_discount desc";
            } else if ($scope.selectedFilter == "Price L to H") {
                $scope.sortType = "price_after_discount asc";
            } else if ($scope.selectedFilter == "Name") {
                $scope.sortType = "title desc";
            }else if ($scope.selectedFilter == "") {
                $scope.sortType = "id desc";
            }

            $scope.isFirst = 3;
            $scope.lastpage = 0;
            $scope.resultList = [];
            $scope.sortListCount();
        }
    }, true);

    $scope.banImg = $scope.category + "_" + $scope.subtype;
    $scope.list = [];
    $scope.isLoaded = false;

    if($scope.category.match("less_")) {
        $scope.resultfor = "All";
    }
    else if($scope.category.match("greater_")){
        $scope.resultfor = "All";
    } 
    else if($scope.category.match("purity_")){
        $scope.resultfor = "All";
    }
    else if($scope.category.match("wt_")){
        $scope.resultfor = "All";
    }
    else if($scope.category.match("weight_lt_")){
        $scope.resultfor = "All";
    }
    else if($scope.category.match("weight_gt_")){
        $scope.resultfor = "All";
    }
    else if($scope.category.match("gender_")){
        $scope.resultfor = "All";
    }
    else 
    {
        $scope.resultfor = $scope.category;
    }

    var el = angular.element(document.querySelector('#loadMore'));
    el.attr('tagged-infinite-scroll', 'loadMore()');
    $compile(el)($scope);
    $scope.lastpage = 0;
    $scope.resultList = [];
    $scope.isFirst = 1;


    $scope.loadMore = function () {

        $scope.fetching = true; 

        if ($scope.isFirst == 1 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.allarr )>-1 && typeof $scope.allarr !== 'undefined' && $scope.allarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.allarr );
                $scope.allarr.splice(pos, 1);

                $http({
                    method: 'GET',
                    url : API_URL + 'getProducts',
                    params: {
                        c: $scope.category,
                        type: $scope.subtype,
                        page: $scope.lastpage
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                        $scope.isLoaded = true;
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        } else if ($scope.isFirst == 2 && $scope.disabled == false) {
            if($.inArray( $scope.lastpage, $scope.filterarr )>-1 && typeof $scope.filterarr !== 'undefined' && $scope.filterarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.filterarr );
                $scope.filterarr.splice(pos, 1);

                $http({
                    method: 'GET',
                    url : API_URL + 'filterPrice',
                    params: {
                        page: $scope.lastpage,
                        c: $scope.category,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {

                        $scope.disabled = true; 
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            }

        } else if ($scope.isFirst == 3 && $scope.disabled == false) {
            
            if($.inArray( $scope.lastpage, $scope.sortarr )>-1 && typeof $scope.sortarr !== 'undefined' && $scope.sortarr.length > 0)
            {
                var pos = $.inArray( $scope.lastpage, $scope.sortarr );
                $scope.sortarr.splice(pos, 1);

                $http({
                    method: 'GET',
                    url : API_URL + 'sortList',
                    params: {
                        p: $scope.sortType,
                        page: $scope.lastpage,
                        c: $scope.category,
                        isGold: $scope.isGold,
                        isWhiteGold: $scope.isWhiteGold,
                        isPlatinum: $scope.isPlatinum,
                        isRoseGold: $scope.isRoseGold,
                        isSilver: $scope.isSilver,
                        is22: $scope.is22,
                        is18: $scope.is18,
                        is14: $scope.is14,
                        isOther: $scope.isOther,
                        isCasual: $scope.isCasual,
                        isBridal: $scope.isBridal,
                        isFashion: $scope.isFashion,
                        jewellers: JSON.stringify($scope.jewellers),
                        type: $scope.subtype,
                        s: $scope.demo1.min,
                        e: $scope.demo1.max
                    }
                }).then(function successCallback(response){
                    $scope.fetching = false;
                    if (response.data.length) {
                        $scope.resultList = $scope.resultList.concat(response.data);
                        $scope.lastpage += 1;
                        $scope.isLoaded = true;
                    } else {
                        $scope.disabled = true; 
                    }
                },function errorCallback(response){
                    console.log(response.data);
                });
            } 
        }
    };


    $scope.filter = function () {  
        $scope.isFirst = 2;
        $scope.lastpage = 0;
        $scope.resultList = [];
        $scope.isFilter = false;
        $scope.filterPriceCount();
    }

    $scope.clearFilter = function () {
        $scope.lastpage = 0;
        $scope.isFilter = false;
        $scope.resetFilter();
    }
}]);

app.controller("productCtrl", ["$scope", "$http", "$stateParams", "$rootScope", "$location", '$timeout', 'FileUploader','API_URL', function ($scope, $http, $stateParams, $rootScope, $location, $timeout, FileUploader,API_URL) {
    $scope.imagename = [];
    $scope.resetcustomize = function () {
        $scope.customizef = {};
      }
    $scope.resetcustomize();

  $scope.saveCustomizeform = function(){
    $scope.imagename = [];
      waitingDialog.show();
      if($scope.uploader.queue.length > 0)
      {
        $scope.uploader.uploadAll();
      }
      else
      {
        $scope.saveCustomizeformData();
      }
  };

  $scope.saveCustomizeformData = function(){
      var url = API_URL + 'saveCustomizeformData';
      $http({
          method: 'POST',
          url: url,
          params : {
              name: $scope.customizef.cmname,
              email: $scope.customizef.cmemail,
              phone: $scope.customizef.cmphone,
              comment: $scope.customizef.cmcomment,
              imagename: JSON.stringify($scope.imagename)
          }
      }).then(function successCallback(response){
          waitingDialog.hide();
          if(response.data === 'success')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeSuccess").modal('show');
              $timeout(function () {
                  $("#customizeSuccess").modal('hide');
              }, 2000);
          }
          else if(response.data === 'error')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeError").modal('show');
              $timeout(function () {
                  $("#customizeError").modal('hide');
              }, 2000);
          }
      },function errorCallback(response){
          console.log(response.data);
          waitingDialog.hide();
          $scope.imagename = [];
          $scope.uploader.clearQueue();
          $scope.resetcustomize();
          $("#CustomizeRing").modal('hide');

          $("#customizeError").modal('show');
          $timeout(function () {
              $("#customizeError").modal('hide');
          }, 2000);
      });
  };

    // start of file upload code
    var uploader = $scope.uploader = new FileUploader({
        url: API_URL + 'uploadimages'
    });

    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        $scope.imagename.push(response.filename);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
        $scope.saveCustomizeformData();
    };

    //console.info('uploader', uploader);

    // end of file upload code
	
	$scope.buyNow = function (s) {
        
        if (s.category == 'Bracelets') {
            if (s.bracelet_size == null) {

                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        $location.path('i/checkout');
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    $location.path('i/checkout');
                } 
            }

        } else if (s.category == 'Rings') {
            if (s.ring_size == null) {

                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        $location.path('i/checkout');
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    $location.path('i/checkout');
                } 
            }

        } else if (s.category == 'Bangles') {
            if (s.bangle_size == null) {
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(s);
        
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                    $http({
                        method: 'POST',
                        url : API_URL + 'savetocart',
                        data: {
                            items: JSON.stringify($rootScope.cart),
                            uid: $rootScope.uid
                        }
                    }).then(function successCallback(response){
                        $location.path('i/checkout');
                    },function errorCallback(response){
                        console.log(response.data);
                    });
                }
                else
                {              
                    $location.path('i/checkout');
                } 
            }

        } else {
            $rootScope.addToCart(s);
        
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    $location.path('i/checkout');
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else
            {              
                $location.path('i/checkout');
            }
        }               
    }

    $scope.customize = function (p) {
		$("#CustomizeRing").modal('show');
    }
    
    $scope.zoomImage = function (i) {
        $scope.isZoom = true;
    }

    $scope.review = {
        pid: null,
        uid: null,
        name: null,
        quality: null,
        price: null,
        value: null,
        review: null
    }
    $scope.addReview = function () {

        if ($rootScope.authenticated) {
            $scope.review.pid = $stateParams.id;
            $scope.review.uid = $rootScope.uid;
            $scope.review.name = $rootScope.name;

            if ($scope.review.price == null || $scope.review.price == null || $scope.review.value == null || $scope.review.value == null || $scope.review.quality == null || $scope.review.quality == "" || $scope.review.review == null || $scope.review.review == "") {
            
                $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
            }
            else
            {
                $http({
                    method: 'POST',
                    url : API_URL + 'addReview',
                    params : {
                    review:JSON.stringify($scope.review)
                        }
                }).then(function successCallback(response){
                   $scope.getReviews();
                    $("#reviewSuccess").modal('show');

                    $timeout(function () {
                        $("#reviewSuccess").modal('hide');
                    }, 2000);
                },function errorCallback(response){
                    $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
                });
            }

        } else {
            $("#login").modal('show');

            $timeout(function () {
                $("#login").modal('hide');
            }, 2000);
        }
    }
    $scope.getCumulativeReview = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getCumulativeReview',
            params : {pid:$stateParams.id}
        }).then(function successCallback(response){
            $scope.cummReview = response.data[0];
            $scope.cummReview.v == null ? $scope.v = 0 : $scope.v = parseFloat($scope.cummReview.v);
            $scope.cummReview.q == null ? $scope.q = 0 : $scope.q = parseFloat($scope.cummReview.q);
            $scope.cummReview.p == null ? $scope.price = 0 : $scope.price = parseFloat($scope.cummReview.p);
            $scope.totalRating = ($scope.q + $scope.price + $scope.v) / 3;
        },function errorCallback(response){
            $("#reviewCummErr").modal('show');

            $timeout(function () {
                $("#reviewCummErr").modal('hide');
            }, 2000);
        });
    }
    $scope.getReviews = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getReviewList',
            params : {pid:$stateParams.id}
        }).then(function successCallback(response){
            $scope.reviewList = response.data;
            $scope.getCumulativeReview();
        },function errorCallback(response){
            $("#reviewListErr").modal('show');

            $timeout(function () {
                $("#reviewListErr").modal('hide');
            }, 2000);
        });
    }
    $scope.getReviews();

    $scope.ratingQuantity = 5;
    $scope.ratingPrice = 3;
    $scope.ratingValue = 2;
    $scope.rateFunction = function (rating) {
        alert('Rating selected - ' + rating);
    };

    $scope.sendAlert = function (email, phone, size) {
        $http({
            method: 'GET',
            url : API_URL + 'setAlert',
            params : {
                    phone:phone,
                    id:$scope.p.id,
                    email:email,
                    size:size
                }
        }).then(function successCallback(response){
            $('#customizeSuccess').modal('show');
            $timeout(function () {
                $('#customizeSuccess').modal('hide');

            }, 2000);
            $scope.isAlert = false;
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });    
    }

    $scope.isOutOfStock = false;
    $scope.getProductVariant = function (v, type) {
        
        $http({
            method: 'GET',
            url : API_URL + 'getProductVariant',
            params : {
                    sku:$scope.p.VC_SKU,
                    v:v,
                    type:type
                }
        }).then(function successCallback(response){
            if (response.data == "") {
                $scope.isOutOfStock = true;
            } else {
                $scope.p.id = response.data[0].id;
                $scope.isOutOfStock = false;
                $scope.p.VC_SKU = response.data[0].VC_SKU;
                $scope.p.VC_SKU_2 = response.data[0].VC_SKU_2;
                $scope.p.price_after_discount = response.data[0].price_after_discount;
                $scope.p.price_before_discount = response.data[0].price_before_discount;
                $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
                $scope.img_count = parseInt(response.data[0].img_count);
            }
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    $scope.productId = $stateParams.id;

    $('a[data-toggle="collapse"]').on('click', function () {

        var objectID = $(this).attr('href');

        if ($(objectID).hasClass('in')) {
            $(objectID).collapse('hide');
        } else {
            $(objectID).collapse('show');
        }
    });
    $scope.getProduct = function () {
        
        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:$scope.productId}
        }).then(function successCallback(response){
            $scope.p = response.data[0];
            $scope.p.ring_size = null;
            $scope.p.bangle_size = null;
            $scope.p.bracelet_size = null;
            $scope.getSimilarProducts();
            $scope.getSimilarJewellerProducts();
            $scope.getJewellerInformation();
            $scope.getProductSize();
            $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
            $scope.img_count = parseInt(response.data[0].img_count);
            
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    $scope.getProduct();
    
    $scope.getProductSize = function (){
        var category = $scope.p.category;
        var sizetype = '';
        if(category === 'Rings'){
            sizetype = 'ring_size';
        }else if(category === 'Bangles'){
            sizetype = 'bangle_size';
        }else if(category === 'Bracelets'){
            sizetype = 'bracelet_size';
        }
        
        if(sizetype != null && sizetype != ''){
            $http({
            method: 'GET',
            url : API_URL + 'getProductSize',
            params : {
                    sku:$scope.p.VC_SKU,
                    category:category,
                    sizetype:sizetype
                }
            }).then(function successCallback(response){
                $scope.psize = response.data;
            },function errorCallback(response){
                console.log(response.data);
            });   
        }
    }
    
    $scope.getSimilarJewellerProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarJewellerProducts',
            params : {s:$scope.p.supplier_name}
        }).then(function successCallback(response){
            $scope.list = response.data;
        },function errorCallback(response){            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $scope.getJewellerInformation = function () {
      
        $http({
            method: 'GET',
            url : API_URL + 'getJewellerInformation',
            params : {
                    s:$scope.p.jeweller_id
                }
        }).then(function successCallback(response){
            $scope.jeweller = response.data[0];
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }
    $scope.getSimilarProducts = function () {
        $http({
            method: 'GET',
            url : API_URL + 'getSimilarProductsByCat',
            params : {c:$scope.p.category}
        }).then(function successCallback(response){
            $scope.same_list = response.data;

            $('#etalage').etalage({
                thumb_image_width: 360,
                thumb_image_height: 360,
                source_image_width: 900,
                source_image_height: 900,
                show_hint: true,
                click_callback: function (image_anchor, instance_id) {

                },
                zoom_area_width: 360,
                zoom_area_height: 360,
                source_image_width: 700,
                source_image_height: 700
            });
                
        },function errorCallback(response){
            console.log(response.data);
            
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);

        });
    }

    }]);
app.filter('num', function () {
    return function (input) {
        return parseFloat(input, 10);
    };
});

app.directive('vivoHeader', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {

            $rootScope.dshow = function(){
                alert('hello');
            }

            $rootScope.openMenuItem = function (l) {
                alert(l);
            };

            // default options (all of them)
            var options = {
                    maxWidth: 300,
                    speed: 0.2,
                    animation: 'ease-out',
                    topBarHeight: 56,
                    modifyViewContent: true,
                    useActionButton: true
                }
                
            $rootScope.openSearchSm = function () {

                $("#topbarsm").animate({
                    top: '60px'
                }, 1000);

            }
            $rootScope.closeSearchSm = function () {

                $("#topbarsm").animate({
                    top: '0px'
                }, 1000);
            }
            $rootScope.openSearchXs = function () {

                $("#topbarxs").animate({
                    top: '77px'
                }, 500);

            }
            $rootScope.closeSearchXs = function () {

                $("#topbarxs").animate({
                    top: '-500px'
                }, 500);
            }
            $rootScope.isSortSmOpen = false;
            $rootScope.isSortXsOpen = false;
            $rootScope.openSortSm = function () {
                if ($rootScope.isSortSmOpen) {
                    $rootScope.closeSortSm();
                } else {
                    $rootScope.isSortSmOpen = true;
                    $("#sortsm").animate({
                        bottom: '43px'
                    }, 1000);
                }

            }
            $rootScope.closeSortSm = function () {
                $rootScope.isSortSmOpen = false;
                $("#sortsm").animate({
                    bottom: '0px'
                }, 1000);
            }
            $rootScope.openSortXs = function () {

                if ($rootScope.isSortXsOpen) {
                    $rootScope.closeSortXs();
                } else {
                    $rootScope.isSortXsOpen = true;
                    $("#sortxs").animate({
                        bottom: '44px'
                    }, 500);
                }
            }
            $rootScope.closeSortXs = function () {
                $rootScope.isSortXsOpen = false;
                $("#sortxs").animate({
                    bottom: '0px'
                }, 500);
            }

        },
        link: function ($scope, $rootScope, Data, $location) {

            if (localStorage.getItem('vivo-cart') != "" && localStorage.getItem('vivo-cart') != null && localStorage.getItem('vivo-cart') != undefined) {
                $rootScope.cart = JSON.parse(localStorage.getItem('vivo-cart'));
            } else {
                $rootScope.cart = null;
            }
        },
        templateUrl: 'm-partials/vivo-header.html'
    };
});

app.directive('vivoFooter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {

        },
        link: function ($scope, $rootScope, Data, $location) {

        },
        templateUrl: 'm-partials/vivo-footer.html'
    };
});

app.directive('vivoProductList', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'm-partials/vivo-product-list.html'
    };
});

app.directive('featuredProducts', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'm-partials/featured-products.html'
    };
});

app.directive('vivoFilter', function () {
    return {
        restrict: 'E',
        controller: function ($scope, $rootScope, Data, $location) {
        },
        link: function ($scope, $rootScope, Data, $location) {
        },
        templateUrl: 'm-partials/vivo-filter.html'
    };
});

app.directive('errSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.bind('error', function() {
            if (attrs.src != attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
          
          attrs.$observe('ngSrc', function(value) {
            if (!value && attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
        }
      }
    });

app.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
}]);

app.directive('starRating',
    function () {
        return {
            restrict: 'A',
            template: '<ul class="rating"> <li ng-repeat="star in stars" ng-class="star" ng-click= "toggle($index)"><i class = "fa fa-star-o"></i></li></ul>',
            scope: {
                ratingValue: '=',
                max: '=',
                onRatingSelected: '&'
            },
            link: function (scope, elem, attrs) {
                var updateStars = function () {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({
                            filled: i < scope.ratingValue
                        });
                    }
                };

                scope.toggle = function (index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating: index + 1
                    });
                };

                scope.$watch('ratingValue',
                    function (oldVal, newVal) {
                        if (newVal) {
                            updateStars();
                        }
                    }
                );
            }
        };
    }
);