! function () {
    "use strict";

    function t(t, e) {
        return function () {
            return t.apply(e, arguments)
        }
    }

    function e(t, e, n) {
        var o;
        if (arguments.length < 3 && (n = this), t)
            if (t.length)
                for (o = 0; o < t.length; o++) e.call(n, o, t[o]);
            else
                for (o in t) e.call(n, o, t[o])
    }

    function n(t, n) {
        var o = t instanceof Array ? [] : {};
        return e(t, function (t, e) {
            o[t] = n(e, t)
        }), o
    }

    function o(t, e, n, o) {
        var r = document.createElement("form");
        r.setAttribute("action", t), n && r.setAttribute("method", n), o && r.setAttribute("target", o), e && (r.innerHTML = i(e)), document.documentElement.appendChild(r), r.submit(), r.parentNode.removeChild(r)
    }

    function i(t, n) {
        if ("object" == typeof t) {
            var o = "";
            return e(t, function (t, e) {
                n && (t = n + "[" + t + "]"), o += i(e, t)
            }), o
        }
        return '<input type="hidden" name="' + n + '" value="' + t + '">'
    }

    function r(t, e, n, o) {
        return "number" == typeof o ? setTimeout(function () {
            r(t, e, n)
        }, o) : ("string" == typeof t && (t = e[t]), "function" == typeof t ? (e || (e = this), 3 === arguments.length ? t.call(e, n) : t.call(e)) : void 0)
    }

    function a(t) {
        for (var e, n = ""; t;) e = t % 62, n = S[e] + n, t = Math.floor(t / 62);
        return n
    }

    function s() {
        var t, n = a(((new Date).getTime() - 13885344e5).toString() + ("000000" + Math.floor(1e6 * Math.random())).slice(-6)) + a(Math.floor(238328 * Math.random())) + "0",
            o = 0;
        return e(n, function (e) {
            t = I[n[n.length - 1 - e]], (n.length - e) % 2 && (t *= 2), t >= 62 && (t = t % 62 + 1), o += t
        }), t = o % 62, t && (t = S[62 - t]), n.slice(0, 13) + t
    }

    function c(t, e) {
        var n = this.id;
        n && /^rzp_l/.test(this.options.key) && setTimeout(function () {
            var o = {
                context: {
                    direct: !0
                },
                anonymousId: n,
                event: t
            };
            e && (e = O.clone(e), "init" === t && (e = l(e), e.medium = F.medium, e.context = F.context, e.ua = P), o.properties = e);
            var i = new XMLHttpRequest;
            i.open("post", "https://api.segment.io/v1/track", !0), i.setRequestHeader("Content-type", "application/json"), i.setRequestHeader("Authorization", "Basic " + R("vz3HFEpkvpzHh8F701RsuGDEHeVQnpSj:")), i.send(JSON.stringify(o))
        })
    }

    function u(t, n) {
        var o = !1,
            i = {};
        return e(n || D.defaults, function (e, n) {
            var r = t[e],
                a = typeof n,
                s = typeof r;
            s === a && (r && "object" === s && (r = u(r, n) || n), r !== n && (o = !0, i[e] = r))
        }), 1 === arguments.length && (o = !0, F.setNotes(i, t)), o ? i : void 0
    }

    function l(t) {
        return t = u(t), delete t.method, F.isBase64Image(t.image) && (t.image = "base64"), t.amount && (t.amount = parseInt(t.amount, 10)), {
            options: t
        }
    }

    function d(t) {
        throw new Error(t)
    }

    function f(t, e) {
        return "object" == typeof t ? t ? (e && "object" == typeof e || (e = {}), n(t, function (t, n) {
            return f(t, e[n])
        })) : "boolean" == typeof e ? e : t : "string" == typeof t && "undefined" != typeof e ? String(e) : typeof t == typeof e ? e : t
    }

    function p(t) {
        e(["key", "amount"], function (e, n) {
            t[n] || d("No " + n + " passed.")
        })
    }

    function h(t) {
        var n;
        e(t, function (t, e) {
            n = r(U[t], null, e), "string" == typeof n && d("Invalid " + t + " (" + n + ")")
        })
    }

    function m(t) {
        t && "object" == typeof t || d("Invalid options"), h(t);
        var e = f(D.defaults, t);
        try {
            var n = t.modal.backdropClose;
            "boolean" == typeof n && (e.modal.backdropclose = n)
        } catch (o) {}
        if (F.setNotes(e, t), "boolean" == typeof t.redirect) {
            var i = t.redirect;
            e.redirect = function () {
                return i
            }
        }
        return t.parent && (e.parent = t.parent), F.setCommunicator(e), e
    }

    function v() {
        return W.meta || (W.meta = _("head meta[name=viewport]")), W.meta
    }

    function g(t) {
        t && t.remove();
        var e = v();
        e && _("head").appendChild(e)
    }

    function y() {
        J.overflow = W.overflow
    }

    function b(t) {
        if (t.image && "string" == typeof t.image) {
            if (F.isBase64Image(t.image)) return;
            if (t.image.indexOf("http")) {
                var e = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : ""),
                    n = "";
                "/" !== t.image[0] && (n += location.pathname.replace(/[^\/]*$/g, ""), "/" !== n[0] && (n = "/" + n)), t.image = e + n + t.image
            }
        }
    }

    function w(t) {
        return t.key ? F.makeUrl() + "checkout?key_id=" + t.key : F.makeUrl(!0) + "checkout.php"
    }

    function k(t) {
        var n = {},
            o = {
                context: location.href,
                options: n,
                config: L,
                id: t.id
            };
        return e(t.options, function (t, e) {
            "function" != typeof e && (n[t] = e)
        }), e(t.modal.options, function (e, n) {
            t.options.modal[e] = n
        }), n.redirect = !!t.options.redirect(), n.parent && (o.embedded = !0), delete n.parent, b(n), o
    }

    function C(t) {
        try {
            K.style.background = t
        } catch (e) {}
    }

    function N(t) {
        return t ? (this.getEl(t.options), this.openRzp(t)) : void this.getEl(D.defaults)
    }

    function A() {
        var t = {};
        e($.attributes, function (e, n) {
            var o = n.name;
            /^data-/.test(o) && (o = o.replace(/^data-/, ""), t[o] = n.value)
        }), V(t);
        var n = $.getAttribute("data-amount");
        n && n.length > 0 && (t.handler = X, Z(D(t)))
    }

    function x() {
        var t = document.createElement("div");
        t.className = "razorpay-container";
        var n = t.style,
            o = {
                zIndex: "99999",
                position: q ? "absolute" : "fixed",
                top: 0,
                display: "none",
                left: 0,
                height: "100%",
                width: "100%",
                "-webkit-transition": "0.2s ease-out top",
                "-webkit-overflow-scrolling": "touch",
                "-webkit-backface-visibility": "hidden",
                "overflow-y": "visible"
            };
        return e(o, function (t, e) {
            n[t] = e
        }), B.appendChild(t), t
    }

    function z() {
        var t = document.createElement("div");
        t.className = "razorpay-backdrop";
        var n = t.style;
        return e({
            "min-height": "100%",
            transition: "0.3s ease-out",
            "-webkit-transition": "0.3s ease-out",
            "-moz-transition": "0.3s ease-out",
            position: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#96000000, endColorstr=#96000000)"
        }, function (t, e) {
            n[t] = e
        }), G.appendChild(t), t
    }

    function E() {
        return !Q && F.supported() && (Q = new N, Q.bind(), G.appendChild(Q.el)), Q
    }
    var O = function (t) {
            return "string" == typeof t ? O(document.querySelector(t)) : this instanceof O ? void(this[0] = t) : new O(t)
        },
        _ = t(document.querySelector, document),
        M = (t(document.querySelectorAll, document), t(document.getElementById, document), t(JSON.stringify, JSON));
    O.prototype = {
        on: function (t, n, o, i) {
            var r = this[0];
            if (r) {
                var a, s = window.addEventListener;
                return a = s ? function (t) {
                    3 === t.target.nodeType && (t.target = t.target.parentNode), n.call(i || this, t)
                } : function (t) {
                    t || (t = window.event), t.target || (t.target = t.srcElement || document), t.preventDefault || (t.preventDefault = function () {
                        this.returnValue = !1
                    }), n.call(i || r, t)
                }, e(t.split(" "), function (t, e) {
                    s ? r.addEventListener(e, a, !!o) : r.attachEvent("on" + e, a)
                }), a
            }
        },
        off: function (t, e, n) {
            window.removeEventListener ? this[0].removeEventListener(t, e, !!n) : window.detachEvent && this[0].detachEvent("on" + t, e)
        },
        prop: function (t, e) {
            var n = this[0];
            return 1 === arguments.length ? n[t] : n ? (n && (n[t] = e), this) : ""
        },
        attr: function (t, n) {
            if ("object" == typeof t) return e(t, function (t, e) {
                this.attr(t, e)
            }, this), this;
            var o = this[0];
            return 1 === arguments.length ? o.getAttribute(t) : (o.setAttribute(t, n), this)
        },
        reflow: function () {
            return this.prop("offsetWidth"), this
        },
        remove: function () {
            try {
                var t = this[0];
                t.parentNode.removeChild(t)
            } catch (e) {}
            return this
        },
        append: function (t) {
            this[0].appendChild(t)
        },
        hasClass: function (t) {
            return (" " + this[0].className + " ").indexOf(" " + t + " ") >= 0
        },
        addClass: function (t) {
            var e = this[0];
            return t && e && (e.className ? this.hasClass(t) || (e.className += " " + t) : e.className = t), this
        },
        removeClass: function (t) {
            var e = this[0];
            if (e) {
                var n = (" " + e.className + " ").replace(" " + t + " ", " ").replace(/^ | $/g, "");
                e.className !== n && (e.className = n)
            }
            return this
        },
        toggleClass: function (t, e) {
            return 1 === arguments.length && (e = !this.hasClass(t)), this[(e ? "add" : "remove") + "Class"](t)
        },
        find: function (t) {
            var e = this[0];
            return e ? e.querySelectorAll(t) : void 0
        },
        css: function (t, e) {
            var n = this.prop("style");
            if (n) {
                if (1 === arguments.length) return n[t];
                try {
                    n[t] = e
                } catch (o) {}
            }
            return this
        },
        hide: function () {
            return this.css("display", "none")
        },
        parent: function () {
            return O(this.prop("parentNode"))
        },
        val: function (t) {
            return arguments.length ? (this[0].value = t, this) : this[0].value
        },
        html: function (t) {
            return arguments.length ? (this[0].innerHTML = t, this) : this[0].innerHTML
        },
        focus: function () {
            if (this[0]) try {
                this[0].focus()
            } catch (t) {}
            return this
        },
        blur: function () {
            if (this[0]) try {
                this[0].blur()
            } catch (t) {}
            return this
        }
    }, O.clone = function (t) {
        return JSON.parse(M(t))
    }, O.post = function (t) {
        var n = new XMLHttpRequest;
        n.open("post", t.url, !0), n.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), t.callback && (n.onreadystatechange = function () {
            4 === n.readyState && t.callback(JSON.parse(n.responseText))
        }, n.onerror = function () {
            t.callback({
                error: {
                    description: "Network error"
                }
            })
        });
        var o = [];
        e(t.data, function (t, e) {
            o.push(t + "=" + e)
        }), n.send(o.join("&"))
    };
    var S = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
        H = S + "+=";
    S = S.slice(52) + S.slice(0, 52);
    var I = {};
    e(S, function (t, e) {
        I[e] = t
    });
    var R = window.btoa;
    R || (R = function (t) {
        var e, n, o, i, r, a;
        for (o = t.length, n = 0, e = ""; o > n;) {
            if (i = 255 & t.charCodeAt(n++), n === o) {
                e += H.charAt(i >> 2), e += H.charAt((3 & i) << 4), e += "==";
                break
            }
            if (r = t.charCodeAt(n++), n === o) {
                e += H.charAt(i >> 2), e += H.charAt((3 & i) << 4 | (240 & r) >> 4), e += H.charAt((15 & r) << 2), e += "=";
                break
            }
            a = t.charCodeAt(n++), e += H.charAt(i >> 2), e += H.charAt((3 & i) << 4 | (240 & r) >> 4), e += H.charAt((15 & r) << 2 | (192 & a) >> 6), e += H.charAt(63 & a)
        }
        return e
    });
    var L, T = (s(), function () {}),
        j = T,
        B = document.body || document.documentElement,
        P = navigator.userAgent,
        q = /iPhone|Android 2\./.test(P);
    L = "object" == typeof window.Razorpay && D && "object" == typeof D.config ? D.config : {
        protocol: "https",
        hostname: "api.razorpay.com",
        version: "v1/"
    };
    var D = window.Razorpay = function (t) {
        return this instanceof D ? void r(this.configure, this, t) : new D(t)
    };
    D.defaults = {
        key: "",
        amount: "",
        currency: "INR",
        handler: function (t) {
            this.callback_url && o(this.callback_url, t, "post")
        },
        notes: {},
        callback_url: "",
        redirect: function () {
            return this.callback_url && /FBAN|\(iP.+((Cr|Fx)iOS|UCBrowser)/.test(P)
        },
        description: "",
        buttontext: "Pay Now",
        parent: null,
        display_currency: "",
        display_amount: "",
        method: {
            netbanking: null,
            card: null,
            wallet: null,
            emi: null
        },
        prefill: {
            name: "",
            contact: "",
            email: "",
            card: {
                number: "",
                expiry: ""
            }
        },
        modal: {
            ondismiss: j,
            onhidden: j,
            escape: !0,
            animation: !0,
            backdropclose: !1
        },
        theme: {
            color: "#00BCD4",
            backdrop_color: "rgba(0,0,0,0.6)",
            image_padding: !0,
            close_button: !0
        },
        signature: "",
        name: "",
        image: ""
    };
    var F = {
            shouldAjax: function (t) {
                return "rzp_live_ILgsfZCZoFIKMb" === t.key_id && F.isFrame && "mobikwik" === t.wallet
            },
            supported: function (t) {
                var e, n = /iPad|iPhone|iPod/.test(navigator.platform);
                return n ? /CriOS/.test(P) ? window.indexedDB || (e = "Please update your Chrome browser or") : /FxiOS|UCBrowser/.test(P) && (e = "This browser is unsupported. Please") : /Opera Mini\//.test(P) && (e = "Opera Mini is unsupported. Please"), e ? (t && (c("unsupported", {
                    message: e,
                    ua: P
                }), alert(e + " choose another browser.")), !1) : !0
            },
            medium: "web",
            context: location.href.replace(/^https?:\/\//, ""),
            setCommunicator: j,
            isBase64Image: function (t) {
                return /data:image\/[^;]+;base64/.test(t)
            },
            defaultError: function () {
                return {
                    error: {
                        description: "Payment cancelled"
                    }
                }
            },
            makeUrl: function (t) {
                var e = L.protocol + "://" + L.hostname + "/";
                return t || (e += L.version), e
            },
            nextRequestRedirect: function (t) {
                return window !== window.parent ? r(D.sendMessage, null, {
                    event: "redirect",
                    data: t
                }) : void o(t.url, t.content, t.method)
            },
            setNotes: function (t, n) {
                e(n.notes, function (e, n) {
                    var o = typeof n;
                    ("string" === o || "number" === o || "boolean" === o) && ("notes" in t || (t.notes = {}), t.notes[e] = n)
                })
            }
        },
        U = {
            key: function (t) {
                return t ? void 0 : ""
            },
            notes: function (t) {
                var n = "";
                if ("object" == typeof t) {
                    var o = 0;
                    if (e(t, function () {
                            o++
                        }), !(o > 15)) return;
                    n = "At most 15 notes are allowed"
                }
                return n
            },
            amount: function (t) {
                var e = parseInt(t, 10),
                    n = String(t);
                if (!e || "number" != typeof e || 100 > e || /\./.test(n)) {
                    var o = "should be passed in paise. Minimum value is 100";
                    return alert("Invalid amount. It " + o), o
                }
            },
            currency: function (t) {
                return "INR" !== t ? "INR is the only supported value." : void 0
            },
            display_currency: function (t) {
                return "USD" !== t && t !== D.defaults.display_currency ? "Only USD is supported" : void 0
            },
            display_amount: function (t) {
                return t = String(t).replace(/([^0-9\. ])/g, ""), t || t === D.defaults.display_amount ? void 0 : ""
            },
            parent: function (t) {
                return t && t.nodeName || "string" == typeof t || t === D.defaults.parent ? void 0 : "Invalid parent"
            }
        };
    D.prototype.configure = function (t) {
        var e, n;
        try {
            n = this.options = m(t), e = n.key, p(n)
        } catch (o) {
            var i = o.message;
            /^rzp_l/.test(e || t.key || "") || alert(i), d(i)
        }
        this instanceof D && (this.id = s(), this.modal = {
            options: {}
        }, c.call(this, "init", t), n.parent && this.open())
    }, D.configure = function (t) {
        D.defaults = m(t)
    };
    var Y = 0,
        J = B.style,
        W = {
            overflow: "",
            meta: null,
            orientationchange: function () {
                this.el.style.height = Math.max(window.innerHeight || 0, 480) + "px"
            },
            scroll: function () {
                if ("number" == typeof window.pageYOffset) {
                    var t, e = G.offsetTop - pageYOffset,
                        n = G.offsetHeight + e;
                    Y < pageYOffset ? n < .2 * innerHeight && 0 > e && (t = pageYOffset + innerHeight - G.offsetHeight) : Y > pageYOffset && e > .1 * innerHeight && n > innerHeight && (t = pageYOffset), "number" == typeof t && (G.style.top = Math.max(0, t) + "px"), Y = pageYOffset
                }
            }
        };
    N.prototype = {
        getEl: function (t) {
            return this.el || (this.el = O(document.createElement("iframe")).attr({
                "class": "razorpay-checkout-frame",
                style: "height: 100%; position: relative; background: none; display: block; border: 0 none transparent; margin: 0px; padding: 0px;",
                allowtransparency: !0,
                frameborder: 0,
                width: "100%",
                height: "100%",
                src: w(t)
            })[0]), this.el
        },
        openRzp: function (t) {
            this.el;
            this.bind();
            var e, n = t.options.parent,
                o = O(n || G);
            t !== this.rzp ? (e = k(t), this.rzp || this.el.parentNode === o[0] || o.append(this.el), this.rzp = t) : e = {
                event: "open"
            }, this.afterLoad(function () {
                this.postMessage(e)
            }), n ? (this.el.removeAttribute("style"), this.embedded = !0, this.afterClose = j) : (o.css("display", "block").reflow(), C(t.options.theme.backdrop_color), this.setMetaAndOverflow())
        },
        close: function () {
            C(""), g(this.$meta), y()
        },
        bind: function () {
            if (!this.listeners) {
                this.listeners = {};
                var t = {
                    message: this.onmessage
                };
                q && (t.orientationchange = W.orientationchange, t.scroll = W.scroll), e(t, function (t, e) {
                    this.listeners[t] = O(window).on(t, e, null, this)
                }, this)
            }
        },
        unbind: function () {
            e(this.listeners, function (t, e) {
                O(window).off(t, e)
            }), this.listeners = null
        },
        setMetaAndOverflow: function () {
            var t = _("head");
            t && (O(v()).remove(), this.$meta = O(document.createElement("meta")).attr({
                name: "viewport",
                content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
            }), t.appendChild(this.$meta[0]), W.overflow = J.overflow, J.overflow = "hidden", q && (scrollTo(0, 0), W.orientationchange.call(this), W.scroll.call(this)))
        },
        postMessage: function (t) {
            t.id = this.rzp.id, t = M(t), this.el.contentWindow.postMessage(t, "*")
        },
        afterLoad: function (t) {
            this.hasLoaded === !0 ? t.call(this) : this.loadedCallback = t
        },
        onmessage: function (t) {
            var e;
            try {
                e = JSON.parse(t.data)
            } catch (n) {
                return
            }
            var o = e.event;
            !t.origin || "frame" !== e.source || "load" !== o && this.rzp && this.rzp.id !== e.id || t.source !== this.el.contentWindow || this.el.getAttribute("src").indexOf(t.origin) || (e = e.data, r("on" + o, this, e), ("dismiss" === o || "fault" === o) && c.call(this.rzp, o, e))
        },
        onload: function () {
            r("loadedCallback", this), this.hasLoaded = !0
        },
        onredirect: function (t) {
            F.nextRequestRedirect(t)
        },
        onsubmit: function (t) {
            var e = window.CheckoutBridge;
            "object" == typeof e && r("onsubmit", e, M(t))
        },
        ondismiss: function () {
            this.close(), r(this.rzp.options.modal.ondismiss)
        },
        onhidden: function () {
            this.afterClose(), r(this.rzp.options.modal.onhidden)
        },
        onsuccess: function (t) {
            this.close(), r("handler", this.rzp.options, t, 200)
        },
        onfailure: function (t) {
            this.ondismiss(), alert("Payment Failed.\n" + t.error.description), this.onhidden()
        },
        onfault: function (t) {
            this.rzp.close(), alert("Oops! Something went wrong.\n" + t), this.afterClose()
        },
        afterClose: function () {
            G.style.display = "none", this.unbind()
        }
    }, F.isCheckout = !0;
    var $ = document.currentScript || function () {
            var t = document.getElementsByTagName("script");
            return t[t.length - 1]
        }(),
        X = function (t) {
            var e = $.parentNode,
                n = document.createElement("div");
            n.innerHTML = i(t), e.appendChild(n), e.onsubmit = j, e.submit()
        },
        V = function (t) {
            var n, o, i, r;
            e(t, function (e, a) {
                i = e.indexOf("."), i > -1 && (o = i, n = e.substr(0, o), r = e.substr(o + 1), t[n] = t[n] || {}, "true" === a ? a = !0 : "false" === a && (a = !1), t[n][r] = a, delete t[e])
            }), t.method && V(t.method)
        },
        Z = function (t) {
            var e = document.createElement("input"),
                n = $.parentElement;
            e.type = "submit", e.value = t.options.buttontext, e.className = "razorpay-payment-button", n.appendChild(e), n.onsubmit = function (e) {
                return e.preventDefault(), t.open(), !1
            }
        },
        G = x(),
        K = z(),
        Q = E();
    D.open = function (t) {
        return D(t).open()
    }, D.prototype.open = function () {
        if (this.options.redirect() || F.supported(!0)) {
            var t = this.checkoutFrame;
            return t || (t = this.options.parent ? new N(this) : E(), this.checkoutFrame = t), t.embedded || t.openRzp(this), t.el.contentWindow || (t.close(), t.afterClose(), alert("This browser is not supported.\nPlease try payment in another browser.")), this
        }
    }, D.prototype.close = function () {
        var t = this.checkoutFrame;
        t && t.postMessage({
            event: "close"
        })
    }, F.validateCheckout = function (t) {
        if ("USD" === t.display_currency) {
            if (t.display_amount = String(t.display_amount).replace(/([^0-9\. ])/g, ""), !t.display_amount) return "display_amount"
        } else if (t.display_currency) return "display_currency"
    }, A()
}();
//# sourceMappingURL=checkout.js.map